package whitelabels.model;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ChildProcessModel {

	public String whitelabelName = "";
	public String postalAddress = "";
	public String arberPixel = "";
	public String child_subject_From_File_Names = "";
	public String image_postaladdress = "";


	public AtomicInteger elasticChildDuplicate = new AtomicInteger(0);

	ArrayList<GroupObject> localGroupList = new ArrayList<GroupObject>();

	public ArrayList<GroupObject> getLocalGroupList() {
		return localGroupList;
	}

	public void setLocalGroupList(ArrayList<GroupObject> localGroupList) {
		this.localGroupList = localGroupList;
	}

}
