package whitelabels.model;

public class UsersData implements Comparable<UsersData> {

	public String isDistanceLessThan70Miles = "";
	public String dashboradFileName = "";
	public String id = "";
	public String searchKeyword = "";
	public String keyword = "";
	public String email = "", zipcode = "", latitude = "", longitude = "", alert_id = "";
	public String[] finalArrayOfKeywordsSearchKeyword = null;
	public String city = "";
	public String state = "";
	public String mail_tester_domain;
	public String mail_tester_api_link;
	public String mail_tester_rating;
	public String mail_tester_debug_link;
	public String mail_tester_whiteLableName;
	public String mail_tester_blacklists_mark;
	public String mail_tester_spamAssassin_mark;
	public String mail_tester_signature_mark;
	public String mail_tester_body_mark;
	public String mail_tester_brokenLinks_mark;
	public String mail_tester_internal_rating_forSort;

	public String child_process_key = "";
	public String userSourceNameForHtml = "";
	public String providerName = "";
	public String categoryProviderForOpenPixels = "";
	public String sendGridCategory = "";
	public String upperLatitude = "";
	public String lowerLatitude = "";
	public String upperLongitude = "";
	public String lowerLongitude = "";
	public String registration_date = "";
	public String advertisement_id = "";
	public String isOpener_Clicker = "0";
	public String locationString = "";
	public String firstName = "";
	public String openTime = "";
	public String scheduleSendForMailgun = "";
	public String radius = "";
	public String domainName = "";
	public String fromDomainName = "";
	public String compainId = "";
	public String jujuChannel = "";
	public String t2Value = "";
	public String liveRamp = "";
	public String sendGridUserName = "";
	public String sendGridUserPassword = "";
	public String campgain_category_mapping_key = "";
	public String errorMessage = "";
	public String country = "";
	public String seeAllMatchingUrl = "";
	public String whitelabel_name = "";
	public String cloudOrJ2cNoKeyword = "cloud";

	public String tempUnsubLink = "";
	public String image_postaladdress = "";

	public String getIsDistanceLessThan70Miles() {
		return isDistanceLessThan70Miles;
	}

	public void setIsDistanceLessThan70Miles(String isDistanceLessThan70Miles) {
		this.isDistanceLessThan70Miles = isDistanceLessThan70Miles;
	}

	public String getDashboradFileName() {
		return dashboradFileName;
	}

	public void setDashboradFileName(String dashboradFileName) {
		this.dashboradFileName = dashboradFileName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAlert_id() {
		return alert_id;
	}

	public void setAlert_id(String alert_id) {
		this.alert_id = alert_id;
	}

	public String[] getFinalArrayOfKeywordsSearchKeyword() {
		return finalArrayOfKeywordsSearchKeyword;
	}

	public void setFinalArrayOfKeywordsSearchKeyword(String[] finalArrayOfKeywordsSearchKeyword) {
		this.finalArrayOfKeywordsSearchKeyword = finalArrayOfKeywordsSearchKeyword;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMail_tester_domain() {
		return mail_tester_domain;
	}

	public void setMail_tester_domain(String mail_tester_domain) {
		this.mail_tester_domain = mail_tester_domain;
	}

	public String getMail_tester_api_link() {
		return mail_tester_api_link;
	}

	public void setMail_tester_api_link(String mail_tester_api_link) {
		this.mail_tester_api_link = mail_tester_api_link;
	}

	public String getMail_tester_rating() {
		return mail_tester_rating;
	}

	public void setMail_tester_rating(String mail_tester_rating) {
		this.mail_tester_rating = mail_tester_rating;
	}

	public String getMail_tester_debug_link() {
		return mail_tester_debug_link;
	}

	public void setMail_tester_debug_link(String mail_tester_debug_link) {
		this.mail_tester_debug_link = mail_tester_debug_link;
	}

	public String getMail_tester_whiteLableName() {
		return mail_tester_whiteLableName;
	}

	public void setMail_tester_whiteLableName(String mail_tester_whiteLableName) {
		this.mail_tester_whiteLableName = mail_tester_whiteLableName;
	}

	public String getMail_tester_blacklists_mark() {
		return mail_tester_blacklists_mark;
	}

	public void setMail_tester_blacklists_mark(String mail_tester_blacklists_mark) {
		this.mail_tester_blacklists_mark = mail_tester_blacklists_mark;
	}

	public String getMail_tester_spamAssassin_mark() {
		return mail_tester_spamAssassin_mark;
	}

	public void setMail_tester_spamAssassin_mark(String mail_tester_spamAssassin_mark) {
		this.mail_tester_spamAssassin_mark = mail_tester_spamAssassin_mark;
	}

	public String getMail_tester_signature_mark() {
		return mail_tester_signature_mark;
	}

	public void setMail_tester_signature_mark(String mail_tester_signature_mark) {
		this.mail_tester_signature_mark = mail_tester_signature_mark;
	}

	public String getMail_tester_body_mark() {
		return mail_tester_body_mark;
	}

	public void setMail_tester_body_mark(String mail_tester_body_mark) {
		this.mail_tester_body_mark = mail_tester_body_mark;
	}

	public String getMail_tester_brokenLinks_mark() {
		return mail_tester_brokenLinks_mark;
	}

	public void setMail_tester_brokenLinks_mark(String mail_tester_brokenLinks_mark) {
		this.mail_tester_brokenLinks_mark = mail_tester_brokenLinks_mark;
	}

	public String getMail_tester_internal_rating_forSort() {
		return mail_tester_internal_rating_forSort;
	}

	public void setMail_tester_internal_rating_forSort(String mail_tester_internal_rating_forSort) {
		this.mail_tester_internal_rating_forSort = mail_tester_internal_rating_forSort;
	}

	public String getChild_process_key() {
		return child_process_key;
	}

	public void setChild_process_key(String child_process_key) {
		this.child_process_key = child_process_key;
	}

	public String getUserSourceNameForHtml() {
		return userSourceNameForHtml;
	}

	public void setUserSourceNameForHtml(String userSourceNameForHtml) {
		this.userSourceNameForHtml = userSourceNameForHtml;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getCategoryProviderForOpenPixels() {
		return categoryProviderForOpenPixels;
	}

	public void setCategoryProviderForOpenPixels(String categoryProviderForOpenPixels) {
		this.categoryProviderForOpenPixels = categoryProviderForOpenPixels;
	}

	public String getSendGridCategory() {
		return sendGridCategory;
	}

	public void setSendGridCategory(String sendGridCategory) {
		this.sendGridCategory = sendGridCategory;
	}

	public String getUpperLatitude() {
		return upperLatitude;
	}

	public void setUpperLatitude(String upperLatitude) {
		this.upperLatitude = upperLatitude;
	}

	public String getLowerLatitude() {
		return lowerLatitude;
	}

	public void setLowerLatitude(String lowerLatitude) {
		this.lowerLatitude = lowerLatitude;
	}

	public String getUpperLongitude() {
		return upperLongitude;
	}

	public void setUpperLongitude(String upperLongitude) {
		this.upperLongitude = upperLongitude;
	}

	public String getLowerLongitude() {
		return lowerLongitude;
	}

	public void setLowerLongitude(String lowerLongitude) {
		this.lowerLongitude = lowerLongitude;
	}

	public String getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}

	public String getAdvertisement_id() {
		return advertisement_id;
	}

	public void setAdvertisement_id(String advertisement_id) {
		this.advertisement_id = advertisement_id;
	}

	public String getIsOpener_Clicker() {
		return isOpener_Clicker;
	}

	public void setIsOpener_Clicker(String isOpener_Clicker) {
		this.isOpener_Clicker = isOpener_Clicker;
	}

	public String getLocationString() {
		return locationString;
	}

	public void setLocationString(String locationString) {
		this.locationString = locationString;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	public String getScheduleSendForMailgun() {
		return scheduleSendForMailgun;
	}

	public void setScheduleSendForMailgun(String scheduleSendForMailgun) {
		this.scheduleSendForMailgun = scheduleSendForMailgun;
	}

	public String getRadius() {
		return radius;
	}

	public void setRadius(String radius) {
		this.radius = radius;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getFromDomainName() {
		return fromDomainName;
	}

	public void setFromDomainName(String fromDomainName) {
		this.fromDomainName = fromDomainName;
	}

	public String getCompainId() {
		return compainId;
	}

	public void setCompainId(String compainId) {
		this.compainId = compainId;
	}

	public String getJujuChannel() {
		return jujuChannel;
	}

	public void setJujuChannel(String jujuChannel) {
		this.jujuChannel = jujuChannel;
	}

	public String getT2Value() {
		return t2Value;
	}

	public void setT2Value(String t2Value) {
		this.t2Value = t2Value;
	}

	public String getLiveRamp() {
		return liveRamp;
	}

	public void setLiveRamp(String liveRamp) {
		this.liveRamp = liveRamp;
	}

	public String getSendGridUserName() {
		return sendGridUserName;
	}

	public void setSendGridUserName(String sendGridUserName) {
		this.sendGridUserName = sendGridUserName;
	}

	public String getSendGridUserPassword() {
		return sendGridUserPassword;
	}

	public void setSendGridUserPassword(String sendGridUserPassword) {
		this.sendGridUserPassword = sendGridUserPassword;
	}

	public String getCampgain_category_mapping_key() {
		return campgain_category_mapping_key;
	}

	public void setCampgain_category_mapping_key(String campgain_category_mapping_key) {
		this.campgain_category_mapping_key = campgain_category_mapping_key;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSeeAllMatchingUrl() {
		return seeAllMatchingUrl;
	}

	public void setSeeAllMatchingUrl(String seeAllMatchingUrl) {
		this.seeAllMatchingUrl = seeAllMatchingUrl;
	}

	public String getWhitelabel_name() {
		return whitelabel_name;
	}

	public void setWhitelabel_name(String whitelabel_name) {
		this.whitelabel_name = whitelabel_name;
	}

	public String getTempUnsubLink() {
		return tempUnsubLink;
	}

	public void setTempUnsubLink(String tempUnsubLink) {
		this.tempUnsubLink = tempUnsubLink;
	}

	public String getImage_postaladdress() {
		return image_postaladdress;
	}

	public void setImage_postaladdress(String image_postaladdress) {
		this.image_postaladdress = image_postaladdress;
	}

	public ParameterModelClass getParameterModelClassObject() {
		return parameterModelClassObject;
	}

	public void setParameterModelClassObject(ParameterModelClass parameterModelClassObject) {
		this.parameterModelClassObject = parameterModelClassObject;
	}

	public String getSparkpost_key() {
		return sparkpost_key;
	}

	public void setSparkpost_key(String sparkpost_key) {
		this.sparkpost_key = sparkpost_key;
	}

	// Paramerts From DB Object
	public ParameterModelClass parameterModelClassObject = null;
	public String sparkpost_key = "";

	// @Override
	// public String toString() {
	// return this.id + "|" + this.keyword + "|" + this.zipcode + "|" +
	// this.email + "||" + this.city + "|" + this.state + "\n";
	// }

	// sorting them in ascending order on the based of their rating
	@Override
	public int compareTo(UsersData o) {

		try {
			float model = Float.valueOf(this.mail_tester_internal_rating_forSort);
			float model1 = Float.valueOf(o.mail_tester_internal_rating_forSort);
			if (model > model1) {
				return 1;
			} else if (model < model1) {
				return -1;
			} else
				return 0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}
}
