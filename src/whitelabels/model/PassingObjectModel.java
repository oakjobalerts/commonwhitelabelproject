package whitelabels.model;


public class PassingObjectModel {

	public UsersData getmUsersData() {
		return mUsersData;
	}

	public void setmUsersData(UsersData mUsersData) {
		this.mUsersData = mUsersData;
	}

	public ReadFeedOrderModel getmReadFeedOrderModel() {
		return mReadFeedOrderModel;
	}

	public void setmReadFeedOrderModel(ReadFeedOrderModel mReadFeedOrderModel) {
		this.mReadFeedOrderModel = mReadFeedOrderModel;
	}

	UsersData mUsersData = null;
	ReadFeedOrderModel mReadFeedOrderModel = null;

}
