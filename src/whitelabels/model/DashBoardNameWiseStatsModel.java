package whitelabels.model;

import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public class DashBoardNameWiseStatsModel {

	// AtomicInteger variables
	public AtomicInteger userProcessingCount = new AtomicInteger(0);
	public AtomicInteger totalEmailCount = new AtomicInteger(0);
	public AtomicInteger numberOfNewJobMails = new AtomicInteger(0);
	public AtomicInteger user_CloudSearch_ProcessingCount = new AtomicInteger(0);
	public AtomicInteger duplicateUser = new AtomicInteger(0);
	public AtomicInteger csUserProcessingCount = new AtomicInteger(0);
	public AtomicIntegerArray jobsCountArray = new AtomicIntegerArray(40);
	public AtomicInteger duplicateUserFromElasticCache = new AtomicInteger(0);
	public LinkedHashMap<String, Integer> jobProvidersJobCountMap = new LinkedHashMap<String, Integer>();
	public LinkedHashMap<String, Integer> jobProvidersJobCountMapForNoKeyword = new LinkedHashMap<String, Integer>();
	public float cloudSearchJobPerc = 0;
	public String totalUsersCount = "0";

}
