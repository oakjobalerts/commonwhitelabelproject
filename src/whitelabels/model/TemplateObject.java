package whitelabels.model;

public class TemplateObject {

	private int templateId;
	transient private int active;
	int templateCount = 0;
	
	
	public TemplateObject(){
		
	}
	public TemplateObject(int tid, int active, int count) {
		templateId = tid;
		active = active;
		templateCount = count;

	}

	public int getTemplateCount() {
		return templateCount;
	}

	public void setTemplateCount(int templateCount) {
		this.templateCount = templateCount;
	}

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

}
