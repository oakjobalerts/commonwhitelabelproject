package whitelabels.model;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class JobsResponseModel {

	public ArrayList<Jobs> getFinalAllJobsList() {
		return finalAllJobsList;
	}

	public void setFinalAllJobsList(ArrayList<Jobs> finalAllJobsList) {
		this.finalAllJobsList = finalAllJobsList;
	}

	public ConcurrentHashMap<String, Double> getApiResponseTimeHashmap() {
		return apiResponseTimeHashmap;
	}

	public void setApiResponseTimeHashmap(ConcurrentHashMap<String, Double> apiResponseTimeHashmap) {
		this.apiResponseTimeHashmap = apiResponseTimeHashmap;
	}

	public ConcurrentHashMap<String, Integer> getApiUserProcessingCountHashmap() {
		return apiUserProcessingCountHashmap;
	}

	public void setApiUserProcessingCountHashmap(ConcurrentHashMap<String, Integer> apiUserProcessingCountHashmap) {
		this.apiUserProcessingCountHashmap = apiUserProcessingCountHashmap;
	}

	ArrayList<Jobs> finalAllJobsList = new ArrayList<Jobs>();
	ConcurrentHashMap<String, Double> apiResponseTimeHashmap = new ConcurrentHashMap<String, Double>();
	ConcurrentHashMap<String, Integer> apiUserProcessingCountHashmap = new ConcurrentHashMap<String, Integer>();

}
