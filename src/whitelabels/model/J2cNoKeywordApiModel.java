package whitelabels.model;

import java.util.List;

public class J2cNoKeywordApiModel {

	String title;
	String date;
	String url;
	String onclick;
	List<String> city;
	List<String> zipcode;
	String description;
	String price;
	String id;
	String industry0;

	public String getTitle () {
		return title;
	}

	public String getDate () {
		return date;
	}

	public String getUrl () {
		return url;
	}

	public String getOnclick () {
		return onclick;
	}

	public List<String> getCity () {
		return city;
	}

	public List<String> getZipcode () {
		return zipcode;
	}

	public String getDescription () {
		return description;
	}

	public String getPrice () {
		return price;
	}

	public String getId () {
		return id;
	}

	public String getIndustry0 () {
		return industry0;
	}

	public void setTitle (String title) {
		this.title = title;
	}

	public void setDate (String date) {
		this.date = date;
	}

	public void setUrl (String url) {
		this.url = url;
	}

	public void setOnclick (String onclick) {
		this.onclick = onclick;
	}

	public void setCity (List<String> city) {
		this.city = city;
	}

	public void setZipcode (List<String> zipcode) {
		this.zipcode = zipcode;
	}

	public void setDescription (String description) {
		this.description = description;
	}

	public void setPrice (String price) {
		this.price = price;
	}

	public void setId (String id) {
		this.id = id;
	}

	public void setIndustry0 (String industry0) {
		this.industry0 = industry0;
	}

	public J2cNoKeywordApiModel () {
		super();
	}

	// title: "Aflac Bilingual Sales Agent",
	// date: "2017-03-27T17:24:49Z",
	// onclick:
	// "j2c_qqdlg_view(778511746, 3644904101, 2261745399, undefined, undefined, undefined, undefined, '&job_loc=Hoboken%2CNJ','62c1c909160c3a043df3f3e7178f4f1c')",
	// company: "Aflac",
	// city: [
	// "Hoboken,NJ",
	// "Astoria,NY",
	// "New York,NY",
	// "Jersey City,NJ",
	// "Brooklyn,NY",
	// "Bronx,NY"
	// ],
	// zipcode: [ ],
	// description:
	// "Imagine being part of a remarkable company. Where you can run your own business.",
	// price: "high",
	// preview: 0,
	// id: "2259731608",
	// industry0: "Bilingual Services / Interpretation / Translation"
}
