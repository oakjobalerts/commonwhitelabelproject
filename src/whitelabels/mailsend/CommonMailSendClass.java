package whitelabels.mailsend;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.commonwhiteLabelproject.main.CommonWhiteLabelMainClass;
import com.commonwhiteLabelproject.main.SettingsClass;
import com.commonwhiteLabelproject.main.Utility;
import com.sendgrid.SendGrid;
import com.sparkpost.exception.SparkPostException;
import com.sparkpost.model.AddressAttributes;
import com.sparkpost.model.RecipientAttributes;
import com.sparkpost.model.TemplateContentAttributes;
import com.sparkpost.model.TransmissionWithRecipientArray;
import com.sparkpost.model.responses.Response;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.RestConnection;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.impl.MultiPartWriter;

import net.spy.memcached.internal.OperationFuture;
import whitelabels.model.ChildProcessModel;
import whitelabels.model.DashBoardNameWiseStatsModel;
import whitelabels.model.GroupObject;
import whitelabels.model.Jobs;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

public class CommonMailSendClass {
	EmailTemplates emailTemplateObject = null;
	public List<String> bccMailList = new ArrayList<String>();

	private BasicAWSCredentials awsCredentialsForSES;

	private AmazonSimpleEmailServiceClient emailSendClient;
	static String awsAccessKeyForSES = "AKIAJB4W4NEOUTCHYQQA";
	static String awsSecretKeyForSES = "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";

	public CommonMailSendClass() {
		awsCredentialsForSES = new BasicAWSCredentials(awsAccessKeyForSES, awsSecretKeyForSES);
		emailSendClient = new AmazonSimpleEmailServiceClient(awsCredentialsForSES);
		emailSendClient.setRegion(Region.getRegion(Regions.US_WEST_2));

		emailTemplateObject = new EmailTemplates();
		bccMailList.add("mangesh@mailtesting.us");
		bccMailList.add("gaurav@mailtesting.us");
		bccMailList.add("pawan@mailtesting.us");
		// bccMailList.add("jason@mailtesting.us");
		bccMailList.add("amit@mailtesting.us");
		bccMailList.add("gagan@mailtesting.us");
		bccMailList.add("raj@mailtesting.us");

	}

	public void sendEmail(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {
		if (userDataObject.sendGridCategory.contains("default value")) {
			sendEmailViaSes(userDataObject, jobsList, localTemplateCount, localGroupCount);
		} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
			sendEmailViaSparkPost(userDataObject, jobsList, localTemplateCount, localGroupCount);
		} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
			userDataObject.tempUnsubLink = "<a title='" + userDataObject.domainName + "' href='%unsubscribe_url%'></a>";
			sendEmailViaMailgun(userDataObject, jobsList, localTemplateCount, localGroupCount);
		} else {
			userDataObject.tempUnsubLink = "<a title='" + userDataObject.domainName + "' href='<UNSUBSCRIBE>' ></a>";
			sendEmailViaSendgrid(userDataObject, jobsList, localTemplateCount, localGroupCount);
		}

	}

	public void sendEmailViaMailgun(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {

		ChildProcessModel childProcessModel = SettingsClass.childProcessModelHashMap.get(userDataObject.child_process_key);
		DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

		ClientConfig cc = new DefaultClientConfig();
		cc.getClasses().add(MultiPartWriter.class);
		Client client = Client.create(cc);
		client.addFilter(new HTTPBasicAuthFilter("api", "key-b8f6462082ffa67a3a56352b14f91523"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + userDataObject.domainName + "/messages");
		webResource.setProperty("domain", "" + userDataObject.domainName + "");
		FormDataMultiPart form = new FormDataMultiPart();
		String html = "";

		// ArrayList<GroupObject> groupList =
		// childProcessModel.getLocalGroupList();
		// GroupObject groupObject = groupSubjectLineObject(userDataObject,
		// jobsList, groupList, localTemplateCount, localGroupCount);

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);
		GroupObject groupObject = groupNameSetup(userDataObject, jobsList, localTemplateCount, localGroupCount, childProcessModel.getLocalGroupList());

		// call different templates for email based on template id
		if (templateObject.getTemplateId() == 1) {

			if (userDataObject.dashboradFileName.toLowerCase().contains("jobvital"))
				html = emailTemplateObject.jobvitalTemplate(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			else
				html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		} else if (templateObject.getTemplateId() == 2) {
		} else if (templateObject.getTemplateId() == 3) {
		} else if (templateObject.getTemplateId() == 4) {
			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 5) {
		} else if (templateObject.getTemplateId() == 6) {
			html = emailTemplateObject.emailTemplate6(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 7) {
			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			if (jobsList.size() > 0) {
				for (int j = 0; j < jobsList.size(); j++) {
					if (!jobsList.get(j).getEmployer().toLowerCase().contains("uber") && !jobsList.get(j).getTitle().toLowerCase().contains("uber")
							&& !jobsList.get(j).getEmployer().toLowerCase().contains("avon") && !jobsList.get(j).getTitle().toLowerCase().contains("avon")) {
						// check for size of job Title (allowed max size 4)
						String jobTitleArray[] = jobsList.get(j).getTitle().split(" ");
						if (jobTitleArray.length < 5) {
							String chesseTemplateSub = jobsList.get(j).getEmployer() + " needs a " + jobsList.get(j).getTitle() + " now";
							groupObject.setSubject(chesseTemplateSub);
							break;
						}

					}
				}
			}
		}

		// Write stats in hashMap
		templateAndGroupIdStats(childProcessModel, userDataObject, groupObject, templateObject);

		String emailStr = "";

		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				form.field("to", userDataObject.email);
				emailStr = userDataObject.email;

			} else {
				form.field("to", SettingsClass.testingEmailId);

				emailStr = SettingsClass.testingEmailId;
			}
		} else {

			form.field("to", userDataObject.email);
			emailStr = userDataObject.email;
			if (SettingsClass.firstEmailFlag || (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails)) {

				for (int i = 0; i < bccMailList.size(); i++) {
					form.field("bcc", bccMailList.get(i));
				}

				SettingsClass.emailCount.set(0);
				SettingsClass.BccMails.getAndIncrement();

				System.out.println("bcc sending.....");
				SettingsClass.firstEmailFlag = false;

			}
		}

		JSONObject jObj = new JSONObject();
		try {
			jObj.put("group_id", groupObject.getGroupId());
			jObj.put("template_id", templateObject.getTemplateId());
			jObj.put("SENTDATE", SettingsClass.dateFormat.format(new Date()));
			jObj.put("REGDATE", userDataObject.registration_date);
			jObj.put("whitelabel_name", userDataObject.whitelabel_name);

			if (userDataObject.providerName.toLowerCase().contains("web"))
				jObj.put("IS_WEB_ALERTS", userDataObject.id);

			// THIS IS FOR EVENING ALERTS USERS
			if (userDataObject.parameterModelClassObject.isForEveningAlerts()) {
				jObj.put("EVENING_ALERTS", "y");
			}
		} catch (JSONException e3) {
			e3.printStackTrace();
		}
		form.field("from", groupObject.getFromName() + "<" + userDataObject.fromDomainName + ">");
		form.field("subject", groupObject.getSubject());
		form.field("v:my-custom-data", jObj.toString());// json value group_id

		// OLD tag
		form.field("o:campaign", userDataObject.compainId);

		//
		form.field("o:tag", userDataObject.compainId);

		form.field("html", html);
		// form.field("text", html);

		String textHtml = emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		// new tags
		form.field("text", textHtml);
		form.field("stripped-html", html);
		form.field("body-html", html);
		form.field("body-plain", textHtml);
		form.field("content-type", MediaType.TEXT_HTML);

		// adding schedule send parameter in the mailgun only in live process
		if (!SettingsClass.isThisTestRun && !SettingsClass.runOrignalTest)
			form.field("o:deliverytime", userDataObject.scheduleSendForMailgun);

		try {
			ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, form);
			if (response.getStatus() == 200) {
				System.out.println("Email sent to " + emailStr + " via mailgun");
				emailSentStats(userDataObject, dashBoardNameWiseStatsModel);

			} else {
				userDataObject.errorMessage = "Email Not Sent";
				// SettingsClass.userNotProcessedList.add(userDataObject);
				System.out.println("The email was not sent via mailgun.");
				try {
					String key = userDataObject.getDashboradFileName().trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;

					String lastNoJobFoundCount = SettingsClass.nojobs_found_users_map.get(key);

					if (lastNoJobFoundCount == null) {
						lastNoJobFoundCount = "1";
					} else {
						lastNoJobFoundCount = String.valueOf(Integer.parseInt(lastNoJobFoundCount) + 1);
					}

					SettingsClass.nojobs_found_users_map.put(key, lastNoJobFoundCount);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public void sendEmailViaSendgrid(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {
		String html = "";
		ChildProcessModel childProcessModel = SettingsClass.childProcessModelHashMap.get(userDataObject.child_process_key);
		DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);
		// ArrayList<GroupObject> groupList =
		// childProcessModel.getLocalGroupList();
		// GroupObject groupObject = groupSubjectLineObject(userDataObject,
		// jobsList, groupList, localTemplateCount, localGroupCount);

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);
		GroupObject groupObject = groupNameSetup(userDataObject, jobsList, localTemplateCount, localGroupCount, childProcessModel.getLocalGroupList());

		if (templateObject.getTemplateId() == 1) {
			html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 2) {
		} else if (templateObject.getTemplateId() == 3) {
		} else if (templateObject.getTemplateId() == 4) {
			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 5) {
		} else if (templateObject.getTemplateId() == 6) {
			html = emailTemplateObject.emailTemplate6(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 7) {
			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			if (jobsList.size() > 0) {
				for (int j = 0; j < jobsList.size(); j++) {
					if (!jobsList.get(j).getEmployer().toLowerCase().contains("uber") && !jobsList.get(j).getTitle().toLowerCase().contains("uber")
							&& !jobsList.get(j).getEmployer().toLowerCase().contains("avon") && !jobsList.get(j).getTitle().toLowerCase().contains("avon")) {
						// check for size of job Title (allowed max size 4)
						String jobTitleArray[] = jobsList.get(j).getTitle().split(" ");
						if (jobTitleArray.length < 5) {
							String chesseTemplateSub = jobsList.get(j).getEmployer() + " needs a " + jobsList.get(j).getTitle() + " now";
							groupObject.setSubject(chesseTemplateSub);
							break;
						}

					}
				}
			}
		}
		// Write stats in hashMap
		templateAndGroupIdStats(childProcessModel, userDataObject, groupObject, templateObject);

		SendGrid sendgrid = new SendGrid(userDataObject.sendGridUserName, userDataObject.sendGridUserPassword);
		SendGrid.Email email = new SendGrid.Email();

		email.setFrom(userDataObject.fromDomainName);
		try {
			email.addUniqueArg("GROUPID", groupObject.getGroupId());
			email.addUniqueArg("TEMPLATEID", String.valueOf(templateObject.getTemplateId()));
			email.addUniqueArg("SENTDATE", String.valueOf(SettingsClass.dateFormat.format(new Date())));
			email.addUniqueArg("REGDATE", String.valueOf(userDataObject.registration_date));
			email.addCategory(userDataObject.sendGridCategory);
			email.addUniqueArg("DOMAIN", userDataObject.domainName);
			email.addUniqueArg("whitelabel_name", userDataObject.whitelabel_name);

			if (userDataObject.providerName.toLowerCase().contains("web"))
				email.addUniqueArg("IS_WEB_ALERTS", userDataObject.id);

			if (userDataObject.parameterModelClassObject.isForEveningAlerts()) {
				email.addUniqueArg("EVENING_ALERTS", "y");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		email.setSubject(groupObject.getSubject());
		email.setFromName(groupObject.getFromName());
		email.setHtml(html);

		String textHtml = emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		email.setText(textHtml);
		String emailStr = "";

		// sending mail to email id..
		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				email.addTo(userDataObject.email);
				emailStr = userDataObject.email;
			} else {
				email.addTo(SettingsClass.testingEmailId);
				emailStr = SettingsClass.testingEmailId;
			}
		} else {
			long scheduledTime = 0;
			try {
				String time = userDataObject.openTime;
				if (!time.equalsIgnoreCase(""))
					scheduledTime = Long.parseLong(time);
			} catch (Exception e) {
			}

			// adding schedule send parameter to the sendrid oakkobject
			if (!SettingsClass.isThisTestRun && !SettingsClass.runOrignalTest)
				email.setSendAt((int) (scheduledTime));

			email.addTo(userDataObject.email);
			emailStr = userDataObject.email;

			if (SettingsClass.firstEmailFlag || (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails)) {
				SettingsClass.firstEmailFlag = false;
				String[] bccArray = new String[bccMailList.size()];
				System.out.println("bcc sending.....");
				for (int i = 0; i < bccMailList.size(); i++) {
					bccArray[i] = bccMailList.get(i);
				}
				email.setBcc(bccArray);
				SettingsClass.BccMails.getAndIncrement();
				SettingsClass.emailCount.set(0);
			}
		}

		try {
			SendGrid.Response response = sendgrid.send(email);
			sendgrid = null;
			email = null;
			if (response.getStatus()) {

				System.out.println("Email sent to " + emailStr + " via sendgrid");
				emailSentStats(userDataObject, dashBoardNameWiseStatsModel);

			} else {
				userDataObject.errorMessage = "Email Not Sent";
				// SettingsClass.userNotProcessedList.add(userDataObject);
				try {
					String key = userDataObject.getDashboradFileName().trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;

					String lastNoJobFoundCount = SettingsClass.nojobs_found_users_map.get(key);

					if (lastNoJobFoundCount == null) {
						lastNoJobFoundCount = "1";
					} else {
						lastNoJobFoundCount = String.valueOf(Integer.parseInt(lastNoJobFoundCount) + 1);
					}

					SettingsClass.nojobs_found_users_map.put(key, lastNoJobFoundCount);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendEmailViaSes(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {
		SendEmailRequest request;
		String html = "";
		ChildProcessModel childProcessModel = SettingsClass.childProcessModelHashMap.get(userDataObject.child_process_key);
		DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

		// ArrayList<GroupObject> groupList =
		// childProcessModel.getLocalGroupList();
		// GroupObject groupObject = groupSubjectLineObject(userDataObject,
		// jobsList, groupList, localTemplateCount, localGroupCount);

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);
		GroupObject groupObject = groupNameSetup(userDataObject, jobsList, localTemplateCount, localGroupCount, childProcessModel.getLocalGroupList());

		if (templateObject.getTemplateId() == 1) {

			if (userDataObject.dashboradFileName.toLowerCase().contains("paperrose"))
				html = emailTemplateObject.paperroseTemplate(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			else
				html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 2) {
		} else if (templateObject.getTemplateId() == 3) {
		} else if (templateObject.getTemplateId() == 4) {
			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 5) {
		} else if (templateObject.getTemplateId() == 6) {
			html = emailTemplateObject.emailTemplate6(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 7) {
			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			if (jobsList.size() > 0) {
				for (int j = 0; j < jobsList.size(); j++) {
					if (!jobsList.get(j).getEmployer().toLowerCase().contains("uber") && !jobsList.get(j).getTitle().toLowerCase().contains("uber")
							&& !jobsList.get(j).getEmployer().toLowerCase().contains("avon") && !jobsList.get(j).getTitle().toLowerCase().contains("avon")) {
						// check for size of job Title (allowed max size 4)
						String jobTitleArray[] = jobsList.get(j).getTitle().split(" ");
						if (jobTitleArray.length < 5) {
							String chesseTemplateSub = jobsList.get(j).getEmployer() + " needs a " + jobsList.get(j).getTitle() + " now";
							groupObject.setSubject(chesseTemplateSub);
							break;
						}

					}
				}
			}

		}
		// Write stats in hashMap
		templateAndGroupIdStats(childProcessModel, userDataObject, groupObject, templateObject);
		String emaiStr = "";

		Destination destination = new Destination();
		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				emaiStr = userDataObject.email;

				destination.withToAddresses(new String[] { userDataObject.email });
			} else {
				emaiStr = SettingsClass.testingEmailId;
				destination.withToAddresses(new String[] { SettingsClass.testingEmailId });
			}
		} else {
			emaiStr = userDataObject.email;
			destination.withToAddresses(new String[] { userDataObject.email });

			if (SettingsClass.firstEmailFlag || (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails)) {
				destination.withBccAddresses(bccMailList);
				System.out.println("bcc sending....");
				SettingsClass.firstEmailFlag = false;
				SettingsClass.emailCount.set(0);
				SettingsClass.BccMails.getAndIncrement();
			}
		}

		String textHtml = emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));

		Content subject = new Content().withData(groupObject.getSubject());
		Content htmlbody = new Content().withData(html);
		Body body = new Body().withHtml(htmlbody);

		Content textHtmlBody = new Content().withData(textHtml);
		body.withText(textHtmlBody);

		// body.withText(htmlbody);

		// Create a message with the specified subject and body.
		com.amazonaws.services.simpleemail.model.Message message = new com.amazonaws.services.simpleemail.model.Message(subject, body);
		request = new SendEmailRequest(groupObject.getFromName() + "<" + userDataObject.fromDomainName + "> ", destination, message);

		try {
			emailSendClient.sendEmail(request);
			System.out.println("Email sent to " + emaiStr + " via Ses");
			emailSentStats(userDataObject, dashBoardNameWiseStatsModel);

			subject = null;
			htmlbody = null;
			body = null;
			request = null;

		} catch (Exception ex) {
			userDataObject.errorMessage = "Email Not Sent";
			// SettingsClass.userNotProcessedList.add(userDataObject);

			try {
				String key = userDataObject.getDashboradFileName().trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;
				String lastNoJobFoundCount = SettingsClass.nojobs_found_users_map.get(key);

				if (lastNoJobFoundCount == null) {
					lastNoJobFoundCount = "1";
				} else {
					lastNoJobFoundCount = String.valueOf(Integer.parseInt(lastNoJobFoundCount) + 1);
				}

				SettingsClass.nojobs_found_users_map.put(key, lastNoJobFoundCount);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("The email was not sent  via Ses.");
			System.out.println("Error message: " + ex.getMessage());
			try {
				if (ex.getMessage().contains("Daily message quota exceeded")) {
					Thread.sleep(1800000);
				} else if (ex.getMessage().contains("Maximum sending rate exceeded")) {
					Thread.sleep(1800000);
				}
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

	}

	public void sendEmailViaSparkPost(UsersData userDataObject, List<Jobs> jobsList, int localTemplateCount, int localGroupCount) {

		String html = "";
		ChildProcessModel childProcessModel = SettingsClass.childProcessModelHashMap.get(userDataObject.child_process_key);
		DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

		// ArrayList<GroupObject> groupList =
		// childProcessModel.getLocalGroupList();
		// GroupObject groupObject = groupSubjectLineObject(userDataObject,
		// jobsList, groupList, localTemplateCount, localGroupCount);

		TemplateObject templateObject = SettingsClass.templateObjectList.get(localTemplateCount);
		GroupObject groupObject = groupNameSetup(userDataObject, jobsList, localTemplateCount, localGroupCount, childProcessModel.getLocalGroupList());

		if (templateObject.getTemplateId() == 1) {
			html = emailTemplateObject.emailTemplate1(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 2) {
		} else if (templateObject.getTemplateId() == 3) {
		} else if (templateObject.getTemplateId() == 4) {
			html = emailTemplateObject.emailTemplate4(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 5) {
		} else if (templateObject.getTemplateId() == 6) {
			html = emailTemplateObject.emailTemplate6(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
		} else if (templateObject.getTemplateId() == 7) {
			html = emailTemplateObject.emailTemplate7(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId()));
			if (jobsList.size() > 0) {
				for (int j = 0; j < jobsList.size(); j++) {
					if (!jobsList.get(j).getEmployer().toLowerCase().contains("uber") && !jobsList.get(j).getTitle().toLowerCase().contains("uber")
							&& !jobsList.get(j).getEmployer().toLowerCase().contains("avon") && !jobsList.get(j).getTitle().toLowerCase().contains("avon")) {
						// check for size of job Title (allowed max size 4)
						String jobTitleArray[] = jobsList.get(j).getTitle().split(" ");
						if (jobTitleArray.length < 5) {
							String chesseTemplateSub = jobsList.get(j).getEmployer() + " needs a " + jobsList.get(j).getTitle() + " now";
							groupObject.setSubject(chesseTemplateSub);
							break;
						}

					}
				}
			}
		}

		// Write stats in hashMap
		templateAndGroupIdStats(childProcessModel, userDataObject, groupObject, templateObject);

		com.sparkpost.Client sparkpostClient = newConfiguredClient(userDataObject);
		TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();
		transmission.setReturnPath(userDataObject.parameterModelClassObject.getSparkpost_bounce_domain());
		List<RecipientAttributes> recipientArray = new ArrayList<RecipientAttributes>();
		RecipientAttributes recipientAttribs = new RecipientAttributes();
		String emailStr = "";

		if (SettingsClass.isThisTestRun) {
			if (SettingsClass.isForMailtester) {
				recipientAttribs.setAddress(new AddressAttributes(userDataObject.email));
				emailStr = userDataObject.email;
			} else {
				recipientAttribs.setAddress(new AddressAttributes(SettingsClass.testingEmailId));
				emailStr = SettingsClass.testingEmailId;
			}
		} else {
			recipientAttribs.setAddress(new AddressAttributes(userDataObject.email));
			emailStr = userDataObject.email;
			if (SettingsClass.firstEmailFlag || (SettingsClass.emailCount.get() >= SettingsClass.bccSendEmailIntervalCount && SettingsClass.BccMails.get() < SettingsClass.BccTotalMails)) {
				String[] bccArray = new String[bccMailList.size()];
				System.out.println("bcc sending.....");
				for (int i = 0; i < bccMailList.size(); i++) {
					bccArray[i] = bccMailList.get(i);
				}
				for (String bcc : bccArray) {
					RecipientAttributes bcc_recipientAttribs1 = new RecipientAttributes();
					AddressAttributes bcc_addressAttribs1 = new AddressAttributes(bcc);
					bcc_addressAttribs1.setHeaderTo(userDataObject.email);
					bcc_recipientAttribs1.setAddress(bcc_addressAttribs1);
					recipientArray.add(bcc_recipientAttribs1);
				}
				SettingsClass.firstEmailFlag = false;

				SettingsClass.BccMails.getAndIncrement();
				SettingsClass.emailCount.set(0);

			}

		}

		recipientArray.add(recipientAttribs);
		Map<String, String> metaData = null;
		try {

			metaData = new HashMap<String, String>();
			metaData.put("GROUPID", String.valueOf(groupObject.getGroupId()));
			metaData.put("TEMPLATEID", String.valueOf(templateObject.getTemplateId()));
			metaData.put("REGDATE", String.valueOf(userDataObject.registration_date));
			metaData.put("SENTDATE", String.valueOf(SettingsClass.dateFormat.format(new Date())));
			metaData.put("DOMAIN", userDataObject.domainName);
			metaData.put("whitelabel_name", userDataObject.whitelabel_name);

			if (userDataObject.providerName.toLowerCase().contains("web"))
				metaData.put("IS_WEB_ALERTS", userDataObject.id);

			if (userDataObject.parameterModelClassObject.isForEveningAlerts()) {
				metaData.put("EVENING_ALERTS", "y");
			}
		} catch (Exception e) {
		}

		// Populate Email Body
		TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
		contentAttributes.setFrom(new AddressAttributes(userDataObject.fromDomainName, groupObject.getFromName(), emailStr));
		contentAttributes.setSubject(groupObject.getSubject());
		contentAttributes.setText(emailTemplateObject.emailTemplateplaintext(userDataObject, jobsList, String.valueOf(groupObject.getGroupId()), String.valueOf(templateObject.getTemplateId())));
		contentAttributes.setHtml(html);
		transmission.setContentAttributes(contentAttributes);
		transmission.setCampaignId(userDataObject.compainId);
		transmission.setMetadata(metaData);

		try {

			transmission.setRecipientArray(recipientArray);
			// sending mail...
			RestConnection connection = new RestConnection(sparkpostClient, RestConnection.defaultApiEndpoint);
			Response response = ResourceTransmissions.create(connection, 0, transmission);

			// Logger.getRootLogger().setLevel(Level.DEBUG);

			// logger.debug("Transmission Response: " + response);

			// System.out.println("response" + response.getStatus()+"");

			if (response.getResponseCode() == 200) {

				System.out.println("Email sent to " + emailStr + " via Sparkpost");
				emailSentStats(userDataObject, dashBoardNameWiseStatsModel);
			} else {
				userDataObject.errorMessage = "Email Not Sent";
				// SettingsClass.userNotProcessedList.add(userDataObject);
				try {
					String key = userDataObject.getDashboradFileName().trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;

					String lastNoJobFoundCount = SettingsClass.nojobs_found_users_map.get(key);

					if (lastNoJobFoundCount == null) {
						lastNoJobFoundCount = "1";
					} else {
						lastNoJobFoundCount = String.valueOf(Integer.parseInt(lastNoJobFoundCount) + 1);
					}

					SettingsClass.nojobs_found_users_map.put(key, lastNoJobFoundCount);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("The email was not sent via Sparkpost.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected static com.sparkpost.Client newConfiguredClient(UsersData userDataObject) {
		try {
			// String API_KEY = "d7f86d1be13186e288a66f2667268885b92b6376";
			com.sparkpost.Client client = new com.sparkpost.Client(userDataObject.sparkpost_key);
			if (StringUtils.isEmpty(client.getAuthKey())) {
				throw new SparkPostException("SPARKPOST_API_KEY must be defined in  CONFIG_FILE .");
			}
			return client;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void emailSentStats(UsersData userDataObject, DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel) {

		try {

			SettingsClass.emailCount.incrementAndGet();
			SettingsClass.newTotalEmailCount.incrementAndGet();
			dashBoardNameWiseStatsModel.totalEmailCount.incrementAndGet();

			if (!SettingsClass.isThisTestRun && userDataObject.parameterModelClassObject.isCylconSubProvider() && !CommonWhiteLabelMainClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
				Object duplicateUserEmailUPTo4MailSentObject = null;
				try {
					duplicateUserEmailUPTo4MailSentObject = SettingsClass.elasticMemCacheObj.get(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon");

				} catch (Exception e) {

					e.printStackTrace();
				}

				if (duplicateUserEmailUPTo4MailSentObject != null) {
					try {
						int count = Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString());
						OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, ++count);
					} catch (Exception e) {
					}
				} else {
					try {
						OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replaceAll(" ", "") + "_Oak_Cylcon", 0, 1);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if (SettingsClass.isForMailtester) {
				// userDataObject.mail_tester_test_email =
				// userDataObject.email;
				userDataObject.mail_tester_domain = userDataObject.domainName;
				userDataObject.mail_tester_api_link = "https://www.mail-tester.com/" + userDataObject.email.split("@")[0] + "&format=json";
				userDataObject.mail_tester_debug_link = "https://www.mail-tester.com/" + userDataObject.email.split("@")[0];
				// MainWhiteLableMailtesterMain.mailtesterDomain_users_List.add(userDataObject);
				// MainWhiteLableMailtesterMain.mailtester_confirmation_map.put(userDataObject.domainName,
				// 1);
				// insertDataInTheMailtester_table(mailtester);

			}

			try {
				if (SettingsClass.isThisTestRun && !SettingsClass.isForMailtester) {
					if (SettingsClass.emailCount.get() >= SettingsClass.numberOfTestingMails) {
						// Utility.updateMemcacheStatsInEnd();
						CommonWhiteLabelMainClass.closeMongoDBConnection();
						System.exit(0);
					}
				} else {
					SettingsClass.memcacheObj.set(userDataObject.email.replaceAll(" ", "") + "_" + userDataObject.keyword.replaceAll(" ", "") + "_" + userDataObject.zipcode.replaceAll(" ", "") + "_"
							+ userDataObject.whitelabel_name.replaceAll(" ", ""), 0, String.valueOf("1")); //
				}
			} catch (Exception e1) {

				e1.printStackTrace();
			}

			try {
				SettingsClass.memcacheObj.set(userDataObject.dashboradFileName.replace(" ", "_") + "_" + SettingsClass.emailSend, 0, String.valueOf(dashBoardNameWiseStatsModel.totalEmailCount)); //
			} catch (Exception e1) {

				e1.printStackTrace();
			}

			try {

				String childUserProcessedKeyStr = userDataObject.child_process_key + "_" + SettingsClass.emailSend;
				Integer childUserProcessed = null;
				synchronized (SettingsClass.child_Process_stats_hashMap) {

					try {
						childUserProcessed = SettingsClass.child_Process_stats_hashMap.get(childUserProcessedKeyStr);
						if (childUserProcessed == null)
							childUserProcessed = 0;
						childUserProcessed++;
						SettingsClass.memcacheObj.set(childUserProcessedKeyStr, 0, String.valueOf(childUserProcessed));
						SettingsClass.child_Process_stats_hashMap.put(childUserProcessedKeyStr, childUserProcessed);
					} catch (Exception e) {
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (SettingsClass.cal.get(Calendar.HOUR_OF_DAY) >= 23 && SettingsClass.cal.get(Calendar.MINUTE) >= 30) {
			SettingsClass.processesIsRunning = 1;
			Utility.updateMemcacheStatsInEnd();
			Utility.killProcess();
		}

	}

	// public GroupObject groupSubjectLineObject(UsersData userDataObject,
	// List<Jobs> jobsList, ArrayList<GroupObject> groupList, int
	// localTemplateCount, int localGroupCount) {
	//
	// if (localGroupCount >= groupList.size())
	// localGroupCount = 0;
	//
	// GroupObject groupObject = new
	// GroupObject(groupList.get(localGroupCount).getGroupId(),
	// groupList.get(localGroupCount).getSubject(),
	// groupList.get(localGroupCount).getFromName());
	//
	// if (userDataObject.city != null && userDataObject.state != null &&
	// !userDataObject.city.toLowerCase().contains("null") &&
	// !userDataObject.state.toLowerCase().contains("null")
	// && !userDataObject.city.toLowerCase().equalsIgnoreCase("") &&
	// !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
	// userDataObject.locationString = " " + userDataObject.city + ", " +
	// userDataObject.state;
	// } else {
	// userDataObject.locationString = " " + userDataObject.zipcode;
	// }
	//
	// // replace both city and state if they both occur in subject line
	// groupObject.setSubject(groupObject.getSubject().replace("CITY, STATE",
	// userDataObject.locationString));
	//
	// // replace city if they only city in subject line
	// if (userDataObject.city != null &&
	// !userDataObject.city.equalsIgnoreCase("") &&
	// !userDataObject.city.toLowerCase().contains("null"))
	// groupObject.setSubject(groupObject.getSubject().replace("CITY",
	// userDataObject.city));
	// else
	// groupObject.setSubject(groupObject.getSubject().replace("CITY",
	// userDataObject.locationString));
	//
	// // replace state if they only state in subject line
	// if (userDataObject.state != null &&
	// !userDataObject.state.equalsIgnoreCase("") &&
	// !userDataObject.state.toLowerCase().contains("null"))
	// groupObject.setSubject(groupObject.getSubject().replace("STATE",
	// userDataObject.state));
	// else
	// groupObject.setSubject(groupObject.getSubject().replace("STATE",
	// userDataObject.locationString));
	//
	// // change radius ,job title , no.of jobs in subject line
	//
	// groupObject.setSubject(groupObject.getSubject().replace("RADIUS",
	// " 30 Miles "));
	//
	// groupObject.setSubject(groupObject.getSubject().replace("JOB TITLE",
	// userDataObject.keyword));
	//
	// groupObject.setSubject(groupObject.getSubject().replace("NUMBER OF",
	// String.valueOf(jobsList.size())));
	//
	// // replace first name on basis of it's presence
	// if (userDataObject.firstName != null &&
	// !userDataObject.firstName.toLowerCase().contains("null") &&
	// !userDataObject.firstName.equalsIgnoreCase(""))
	// groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME",
	// userDataObject.firstName));
	// else
	// groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME ,",
	// ""));
	//
	// groupObject.setSubject(groupObject.getSubject().replace("(current date)",
	// SettingsClass.dateFormetForSubjectLine.format(new Date())));
	//
	// return groupObject;
	//
	// }

	public static GroupObject groupNameSetup(UsersData userDataObject, List<Jobs> jobsArray, int localTemplateCount, int localGroupCount, ArrayList<GroupObject> groupObjectList) {

		String jobTitle = "";
		String jobLocation = "";
		String jobCompany = "";

		if (jobsArray.size() > 0) {
			for (int j = 0; j < jobsArray.size(); j++) {

				if (!jobsArray.get(j).getExactOrSynonym().equalsIgnoreCase("e"))
					break;

				if (!jobsArray.get(j).getEmployer().equalsIgnoreCase("") && !jobsArray.get(j).getEmployer().toLowerCase().contains("uber")
						&& !jobsArray.get(j).getEmployer().toLowerCase().contains("avon")

						&& !jobsArray.get(j).getTitle().toLowerCase().contains("uber") && !jobsArray.get(j).getTitle().toLowerCase().contains("avon")) {
					// check for size of job Title (allowed max size 4)

					String jobTitleArray[] = jobsArray.get(j).getTitle().split(" ");
					if (jobTitleArray.length < 5) {

						jobTitle = jobsArray.get(j).getTitle();
						jobCompany = jobsArray.get(j).getEmployer();
						if (!jobsArray.get(j).getCity().equalsIgnoreCase("") && !jobsArray.get(j).getState().equalsIgnoreCase(""))
							jobLocation = jobsArray.get(j).getCity() + ", " + jobsArray.get(j).getState();
						else if (!jobsArray.get(j).getCity().equalsIgnoreCase(""))
							jobLocation = jobsArray.get(j).getCity();
						else if (!jobsArray.get(j).getState().equalsIgnoreCase(""))
							jobLocation = jobsArray.get(j).getState();

						if (!jobLocation.equalsIgnoreCase(""))
							break;
					}

				}
			}
		}

		if (localGroupCount >= groupObjectList.size())
			localGroupCount = 0;

		GroupObject groupObject = new GroupObject(groupObjectList.get(localGroupCount).getGroupId(), groupObjectList.get(localGroupCount).getSubject(), groupObjectList.get(localGroupCount)
				.getFromName());

		if (userDataObject.city != null && userDataObject.state != null && !userDataObject.city.toLowerCase().contains("null") && !userDataObject.state.toLowerCase().contains("null")
				&& !userDataObject.city.toLowerCase().equalsIgnoreCase("") && !userDataObject.state.toLowerCase().equalsIgnoreCase("")) {
			userDataObject.locationString = userDataObject.city + ", " + userDataObject.state;
		} else {
			userDataObject.locationString = userDataObject.zipcode;
		}

		if (userDataObject.keyword.equalsIgnoreCase("")) {
			groupObject.setSubject("New positions near " + userDataObject.locationString + " - " + SettingsClass.dateFormetForSubjectLine.format(new Date()));
			return groupObject;
		}

		if (jobTitle.equalsIgnoreCase("") && groupObject.getSubject().contains("JOB TITLE")) {
			groupObject.setSubject(userDataObject.keyword + " wanted in " + userDataObject.locationString);
			return groupObject;
		}

		groupObject.setSubject(groupObject.getSubject().replace("JOB TITLE", jobTitle));
		groupObject.setSubject(groupObject.getSubject().replace("JOB LOCATION", jobLocation));
		groupObject.setSubject(groupObject.getSubject().replace("KEYWORD", userDataObject.keyword));
		groupObject.setSubject(groupObject.getSubject().replace("USER LOCATION", userDataObject.locationString));
		groupObject.setSubject(groupObject.getSubject().replace("COMPANY", jobCompany));

		groupObject.setSubject(groupObject.getSubject().replace("CITY, STATE", userDataObject.locationString));

		if (userDataObject.city != null && !userDataObject.city.equalsIgnoreCase("") && !userDataObject.city.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.city));
		else
			groupObject.setSubject(groupObject.getSubject().replace("CITY", userDataObject.locationString));

		if (userDataObject.state != null && !userDataObject.city.equalsIgnoreCase("") && !userDataObject.state.toLowerCase().contains("null"))
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.state));
		else
			groupObject.setSubject(groupObject.getSubject().replace("STATE", userDataObject.locationString));

		groupObject.setSubject(groupObject.getSubject().replace("RADIUS", " 30 Miles "));

		groupObject.setSubject(groupObject.getSubject().replace("NUMBER OF", String.valueOf(jobsArray.size())));

		if (userDataObject.firstName != null && !userDataObject.firstName.toLowerCase().contains("null") && !userDataObject.firstName.equalsIgnoreCase(""))
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME", userDataObject.firstName));
		else
			groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME ,", ""));

		groupObject.setSubject(groupObject.getSubject().replace("FIRST NAME", "YOU"));

		groupObject.setSubject(groupObject.getSubject().replace("(current date)", SettingsClass.dateFormetForSubjectLine.format(new Date())));
		return groupObject;
	}

	public void templateAndGroupIdStats(ChildProcessModel childProcessModel, UsersData userDataObject, GroupObject groupObject, TemplateObject templateObject) {

		try {
			String key = childProcessModel.child_subject_From_File_Names + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();
			Integer templateCountObject = SettingsClass.templateCountHashMap.get(key);
			if (templateCountObject != null)
				SettingsClass.templateCountHashMap.put(key, templateCountObject + 1);
			else
				SettingsClass.templateCountHashMap.put(key, 1);

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			String key = userDataObject.providerName + "_" + userDataObject.child_process_key + "_" + groupObject.getGroupId() + "_" + templateObject.getTemplateId();
			Integer templateCountObject = SettingsClass.doaminCategoryWiseTemplateGroupIdMap.get(key);
			if (templateCountObject != null)
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, templateCountObject + 1);
			else
				SettingsClass.doaminCategoryWiseTemplateGroupIdMap.put(key, 1);
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
