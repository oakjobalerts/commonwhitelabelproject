package com.commonwhiteLabelproject.main;

import java.net.InetSocketAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;

import net.spy.memcached.MemcachedClient;
import whitelabels.model.ChildProcessModel;
import whitelabels.model.TemplateObject;
import whitelabels.model.UsersData;

public class SettingsClass {

	public static List<String> whitelabelList = Arrays.asList("elitejob", "celojobs", "senzajobalerts", "blaze", "vanajobs", "almanacjobs", "fazedjobs", "jobstomyinbox");

	public static int TOTAL_N0_THREAD = 1;
	public static boolean isThisTestRun = true;
	public static boolean runOrignalTest = false;
	public static int numberOfTestingMails = 4;
	// public static String testingEmailId = "kanav@signitysolutions.com";
	public static String testingEmailId = "parveen.k@signitysolutions.in";
	// public static String testingEmailId = "pawan@signitysolutions.co.in";

	public static String filePath = "/home/parveen/Desktop/aws_stats/";
	// public static String filePath = "/home/signity/aws_stats/";

	/*
	 * Extra Variables
	 */
	public static Calendar cal = null;
	public static MemcachedClient memcacheObj;
	public static MemcachedClient elasticMemCacheObj;
	public static MemcachedClient urlShortenElasticMemCacheObj;
	public static AWSCredentials awsCredentials;
	public static AmazonSQSClient sqs;
	public static boolean firstEmailFlag = true;

	/*
	 * SimpleDateFormat format objects
	 */
	public static SimpleDateFormat processTimeDateFormat = new SimpleDateFormat("hh:mm aaa");
	public static SimpleDateFormat dateFormetForSubjectLine = new SimpleDateFormat("MMM d, yyyy");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	public static SimpleDateFormat sentDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

	/*
	 * String variables
	 */
	public static String queuName = "";

	public static String totalUsersCount;
	public static String todayDate = "";
	public static String dashboradFileName = "";
	public static String totalUserKey = "totalUserKey";
	public static String userProcessed = "userProcessed";
	public static String cloudSearchUsersPer = "cloudSearchUsersPer";
	public static String csUserProcessingCount = "csUserProcessingCount";
	public static String emailSend = "emailSend";
	public static String processStartTime = "processStartTime";
	public static String numberofNewJobMailsKey = "numberofNewJobMailsKey";
	public static String jobsCountInMails = "jobsCountInMails";
	public static String groupTemplatesCountKey = "groupTemplatesCountKey";
	public static String duplicateUserKey = "duplicateUserKey";
	public static String JobProvidersjobsCountKey = "JobProvidersjobsCountKey";
	public static String JobProvidersjobsCountKeyForNoKeyword = "JobProvidersjobsCountKeyForNoKeyword";

	public static String perSecondsMails = "perSecondsMails";
	public static String mailCloudJobsPer = "mailCloudJobsPer";
	public static String perSecondUsers = "perSecondUsers";
	public static String child_Process_stats_hashMap_key = "child_Process_stats_hashMap_key";
	public static String average_percentage_NewJobsMailsKey = "average_percentage_NewJobsMailsKey";
	public static String elasticDublicateKey = "elasticDublicateKey";
	public static String elasticDuplicateMemcacheKey = "childElasticDuplicateMemcacheKey";
	public static String domainConfigfilePath = "/var/nfs-93/redirect/mis_logs/Java_process_stats/rdsDomainConfig.txt";
	public static String domainQueueUrl = "https://sqs.us-east-1.amazonaws.com/306640124653/rdsdomainConfig";
	public static String baseQueurl = "https://sqs.us-east-1.amazonaws.com/306640124653/";
	public static Region region = Region.getRegion(Regions.US_EAST_1);
	public static String awsAccessKey = "AKIAJFSBEF3PYJ7SNOCQ";
	public static String awsSecretKey = "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";
	public static String feedOrderFolderName = "FeedOrderFilesForDemo";
	public static int bccSendEmailIntervalCount = 100001;
	public final static int BccTotalMails = 100;
	public static int bccSendEmailIntervalPercentage = 30;
	public static int processesIsRunning = 0;
	public final static String CS = "c";
	public static boolean isForMailtester = false;
	public static String new_jobProviderCountDomainWiseKey = "globalDomainCaterotyjobProviderCount";
	/*
	 * All Lists
	 */
	public static List<String> keywordList = new ArrayList<String>();
	public static ArrayList<TemplateObject> templateObjectList = new ArrayList<TemplateObject>();

	/*
	 * All HashMap
	 */
	// public static Set<String> feedApiListForProviderWiseSourceCountStats;
	public static Map<String, List<String>> apiFeedOrderToDbNameMatchMap;
	public static Map<String, ChildProcessModel> childProcessModelHashMap = new HashMap<String, ChildProcessModel>();
	// public static ConcurrentHashMap<String, Double> apiResponseTimeHashmap =
	// new ConcurrentHashMap<String, Double>();
	// public static ConcurrentHashMap<String, Integer>
	// apiUserProcessingCountHashmap = new ConcurrentHashMap<String, Integer>();
	// public static List<UsersData> userNotProcessedList = new
	// ArrayList<UsersData>();
	public static HashMap<String, Integer> templateCountHashMap = new HashMap<String, Integer>();
	public static HashMap<String, Integer> doaminCategoryWiseTemplateGroupIdMap = new HashMap<String, Integer>();
	public static Map<String, Integer> child_Process_stats_hashMap = Collections.synchronizedMap(new HashMap<String, Integer>());
	public static LinkedHashMap<String, Integer> new_jobProviderCountDomainWise = new LinkedHashMap<String, Integer>();

	/*
	 * For Stopping server
	 */
	public static String ec2AwsAccessKey = "AKIAIJJZZVULVKZNE7RQ";
	public static String ec2AwsSecretKey = "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";
	// New server 98 access and secret key
	public static String ec2_98AwsAccessKey = "AKIAI6ISH4ZK4DMLB7UA";
	public static String ec2_98AwsSecretKey = "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";
	public static String server189InstanceId = "i-9ceb9534";
	public static String server62InstanceId = "i-5d9ceef5";
	public static String server7InstanceId = "i-046daf65535cd1887";
	public static String server235InstanceId = "i-3416539f";
	public static String server109InstanceId = "i-9db6a735";
	public static String server98InstanceId = "i-0c44166521af2ad95";
	public static String killserverFilePath = "/var/nfs-93/redirect/mis_logs/Java_process_stats/";
	public static boolean isProcesskilled = false;

	// AtomicInteger variables
	public static AtomicInteger emptyUserMessageCount = new AtomicInteger(0);
	public static AtomicInteger emptyQueueCount = new AtomicInteger(0);
	public static AtomicInteger errorCount = new AtomicInteger(0);
	public static AtomicInteger topResumeCounter = new AtomicInteger(1);
	public static AtomicInteger adsCounter = new AtomicInteger(1);

	public static AtomicInteger BccMails = new AtomicInteger(0);
	public static AtomicInteger emailCount = new AtomicInteger(0);
	public static AtomicInteger newTotalEmailCount = new AtomicInteger(0);
	public static AtomicInteger newuserProcessingCount = new AtomicInteger(0);
	public static DecimalFormat decFormat = new DecimalFormat("###.###");
	public static AtomicInteger fromMongoDB = new AtomicInteger(0);
	public static AtomicInteger mongoSmsCounter = new AtomicInteger(0);

	SettingsClass() {
		SettingsClass.awsCredentials = new BasicAWSCredentials(SettingsClass.awsAccessKey, SettingsClass.awsSecretKey);
		SettingsClass.sqs = new AmazonSQSClient(SettingsClass.awsCredentials);
		SettingsClass.sqs.setRegion(SettingsClass.region);

		try {
			SettingsClass.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
			if (!isThisTestRun && !CommonWhiteLabelMainClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95"))
				SettingsClass.elasticMemCacheObj = new MemcachedClient(new InetSocketAddress("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211));
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			SettingsClass.urlShortenElasticMemCacheObj = new MemcachedClient(new InetSocketAddress("url-shortner.25s6pq.0001.use1.cache.amazonaws.com", 11211));
		} catch (Exception e) {
		}

		cal = Calendar.getInstance();
		// feedApiListForProviderWiseSourceCountStats = new HashSet<String>();
		// setupApiFeedToDbMap();

		for (int i = 1; i <= 7; i++) {

			if (dashboradFileName.toLowerCase().contains("oakjob") && i == 1)
				continue;

			if (i == 1 || i == 4 || i == 6 || i == 7) {
				TemplateObject object = new TemplateObject();
				object.setActive(1);
				object.setTemplateId(i);
				SettingsClass.templateObjectList.add(object);
			}

		}

		// try {
		// if (!isThisTestRun && !runOrignalTest)
		// TOTAL_N0_THREAD =
		// Integer.parseInt(MainWhiteLabelClass.parameterModelClassObject.getThreadCount());
		// } catch (Exception e) {
		// }

		/*
		 * For Add To EMail Keywords List
		 */

		keywordList.add("Accounting");
		keywordList.add("General Business");
		keywordList.add("Other");
		keywordList.add("Admin Clerical");
		keywordList.add("General Labor");
		keywordList.add("Pharmaceutical");
		keywordList.add("Automotive");
		keywordList.add("Government");
		keywordList.add("Professional Services");
		keywordList.add("Banking");
		keywordList.add("Grocery");
		keywordList.add("Purchasing - Procurement");
		keywordList.add("Biotech");
		keywordList.add("Health Care");
		keywordList.add("QA - Quality Control");
		keywordList.add("Broadcast - Journalism");
		keywordList.add("Hotel - Hospitality");
		keywordList.add("Real Estate");
		keywordList.add("Business Development");
		keywordList.add("Human Resources");
		keywordList.add("Research");
		keywordList.add("Construction");
		keywordList.add("Information Technology");
		keywordList.add("Restaurant - Food Service");
		keywordList.add("Consultant");
		keywordList.add("Installation - Maint - Repair");
		keywordList.add("Retail");
		keywordList.add("Customer Service");
		keywordList.add("Insurance");
		keywordList.add("Sales");
		keywordList.add("Design");
		keywordList.add("Inventory");
		keywordList.add("Science");
		keywordList.add("Distribution - Shipping");
		keywordList.add("Legal");
		keywordList.add("Skilled Labor - Trades");
		keywordList.add("Education - Teaching");
		keywordList.add("Legal Admin");
		keywordList.add("Strategy - Planning");
		keywordList.add("Engineering");
		keywordList.add("Management");
		keywordList.add("Supply Chain");
		keywordList.add("Entry Level - New Grad");
		keywordList.add("Manufacturing");
		keywordList.add("Telecommunications");
		keywordList.add("Executive");
		keywordList.add("Marketing");
		keywordList.add("Training");
		keywordList.add("Facilities");
		keywordList.add("Media - Journalism - Newspaper");
		keywordList.add("Transportation");
		keywordList.add("Finance");
		keywordList.add("Nonprofit - Social Services");
		keywordList.add("Warehouse");
		keywordList.add("Franchise");
		keywordList.add("Nurse");

		// queuName =
		// MainWhiteLabelClass.parameterModelClassObject.getQueueName();
		// whitelabel_processname =
		// MainWhiteLabelClass.parameterModelClassObject.getWhitelabel_name();
		// // userSourceNameForHtml =
		// // MainWhiteLabelClass.parameterModelClassObject.getHtmlSourceName();
		// dashboradFileName =
		// MainWhiteLabelClass.parameterModelClassObject.getDashboardName();

		String suffix = "";

		Calendar calendar = new GregorianCalendar();

		int n = calendar.get(Calendar.DATE);

		switch (n % 10) {
		case 1:
			suffix = "st";
			break;
		case 2:
			suffix = "nd";
			break;
		case 3:
			suffix = "rd";
			break;
		default:
			suffix = "th";
			break;

		}

		if (n >= 11 && n <= 13) {
			suffix = "th";
		}

		SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE, MMMM d'" + suffix + "'  yyyy");
		// SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE, dd MMM, yyyy");

		todayDate = sdf1.format(new Date());

	}

	public static String smsjob_banner = "smsjob_banner";
	public static String smsjob_banner_1 = "smsjob_banner_1";
	public static String ResumeRabbit1_banner = "ResumeRabbit1_banner";
	public static String ResumeRabbit2_banner = "ResumeRabbit2_banner";
	public static AtomicInteger last_email_counter_for_banner = new AtomicInteger(0);
	public static HashMap<String, String> nojobs_found_users_map = new HashMap<String, String>();
	public static String no_jobs_found_users_key = "nojobsfound";

}
