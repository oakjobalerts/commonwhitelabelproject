package com.commonwhiteLabelproject.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.logging.Level;

import net.spy.memcached.internal.OperationFuture;

import org.bson.Document;
import org.json.JSONObject;

import whitelabels.mailsend.CommonMailSendClass;
import whitelabels.model.ApiStatusModel;
import whitelabels.model.ChildProcessModel;
import whitelabels.model.DashBoardNameWiseStatsModel;
import whitelabels.model.GroupObject;
import whitelabels.model.JobProviderJobCountData;
import whitelabels.model.Jobs;
import whitelabels.model.JobsResponseModel;
import whitelabels.model.ParameterModelClass;
import whitelabels.model.PassingObjectModel;
import whitelabels.model.ReadFeedOrderModel;
import whitelabels.model.ScoreBaseFeedModel;
import whitelabels.model.UsersData;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.util.EC2MetadataUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

public class CommonWhiteLabelMainClass {
	/*
	 * External library Objects
	 */
	public static Map<String, ClassLoader> trackLoadedJars = new HashMap<String, ClassLoader>();
	public ClassLoader classLoader;
	public Class<?> jobSearchclass;
	public Method method;
	public CommonMailSendClass mCommonMailSendClass = new CommonMailSendClass();

	/*
	 * otbers variabels
	 */
	public static String instanceId = "";
	public static String startTimeForDashBoard = "";
	public static long processStartTime;
	public static CommonWhiteLabelMainClass mainClassObject = new CommonWhiteLabelMainClass();
	// Gson gson = new GsonBuilder().disableHtmlEscaping().create();

	/*
	 * All Hashmap and lists
	 */
	public static HashMap<String, ParameterModelClass> whiteLabelDomainWiseDataHashMap = new LinkedHashMap<String, ParameterModelClass>();
	public static HashMap<String, DashBoardNameWiseStatsModel> dashboardNameStatsModelHashmap = new LinkedHashMap<String, DashBoardNameWiseStatsModel>();
	public static HashMap<String, ReadFeedOrderModel> feedOrderModelWhiteLabelWise = new LinkedHashMap<String, ReadFeedOrderModel>();
	public static Set<String> providersSet = new HashSet<String>();
	public static ArrayList<String> childDomainMemCacheArrayList = new ArrayList<String>();
	public static HashMap<String, String> doimanWiseHashMap = new LinkedHashMap<String, String>();

	public static boolean readLiveStatsFile = false;
	public static boolean readLiveChildStatsFile = false;

	public static Gson mGson = new GsonBuilder().disableHtmlEscaping().create();

	public static MongoClient mongoClient = null;
	public static String mongoServerIp = "34.233.239.71";
	public static MongoCollection<Document> document = null;
	public static LinkedHashMap<String, String> urlMaxLengthForMongo = new LinkedHashMap<String, String>();

	public static void main(String args[]) {

		startTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());
		processStartTime = System.currentTimeMillis();

		String processName = "BlazeAlerts";

		String emailForTesting = "";
		try {

			if (args != null && args[0] != null) {
				SettingsClass.filePath = "/var/nfs-93/redirect/mis_logs/";
				mongoServerIp = "172.31.50.38";

				processName = args[0];

				String processType = "";
				try {
					processType = args[1].trim();
				} catch (Exception e) {
				}

				if (!processType.equalsIgnoreCase("")) {

					if (processType.equalsIgnoreCase("runtest")) {
						SettingsClass.isThisTestRun = true;
						SettingsClass.runOrignalTest = false;
					} else if (processType.equalsIgnoreCase("runtest_original")) {
						SettingsClass.isThisTestRun = false;
						SettingsClass.runOrignalTest = true;
					} else {
						System.out.println("Run time Parameters not match! Please try again");
						System.exit(0);
					}

				} else {
					SettingsClass.isThisTestRun = false;
					SettingsClass.runOrignalTest = false;
				}
				try {
					emailForTesting = args[2].trim();
				} catch (Exception e) {
					emailForTesting = "";
				}
				if (!emailForTesting.equalsIgnoreCase(""))
					SettingsClass.testingEmailId = emailForTesting;

			} else {
				System.out.println("Run time Parameters not found! Please try again");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("Run time Parameters not found! Please try again");
			// System.exit(0);
		}

		createParametersModelFromDB(processName);
		makeMongoDBCOnnection();
		if (processName.equalsIgnoreCase("")) {
			System.out.println("Run time Parameters not found! Please try again");
			Utility.sendTextSms("Run time parameter found empty please check cron with empty parameter.");
			System.exit(0);
		}
		SettingsClass.dashboradFileName = processName;
		if (!SettingsClass.isThisTestRun)
			instanceId = EC2MetadataUtils.getInstanceId();

		System.out.println(SettingsClass.dashboradFileName);

		loadExtraJarFiles();
		new SettingsClass();
		if (!emailForTesting.equalsIgnoreCase(""))
			SettingsClass.testingEmailId = emailForTesting;
		try {
			ReadApiParametersThread readFileThread = new ReadApiParametersThread();
			new Thread(readFileThread).start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Thread.sleep(1 * 10 * 1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// reading stats from the MemCache.............................
		readStatsFromMemCache();

		if (readLiveStatsFile) {
			readStatsFromFile();
		}

		if (readLiveChildStatsFile) {
			readChildDomainsStatsFromFile();
		}

		try {
			WriteApiResponseTimeInFileThread writeFileThread = new WriteApiResponseTimeInFileThread();
			new Thread(writeFileThread).start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		createDomainWiseHashMap();

		// Start Getting user and process
		getDataFromQueue();

	}

	public static void makeMongoDBCOnnection() {

		try {

			java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);
			// To connect to mongodb server
			mongoClient = new MongoClient(mongoServerIp, 27017);

			// Now connect to your databases
			MongoDatabase db = mongoClient.getDatabase("alerts_shorten_urls");

			System.out.println("Connect to mongo database successfully");

			document = db.getCollection("ShortenUrls");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void insertRecord(String id, String longUrl) {

		if (document == null)
			makeMongoDBCOnnection();

		Document document1 = new Document();
		document1.put("_id", id);

		Document document2 = new Document();
		document2.put("long_url", longUrl);
		document2.put("date", SettingsClass.dateFormat.format(new Date()));

		UpdateOptions updateOptions = new UpdateOptions();
		updateOptions.upsert(true);

		document.updateOne(document1, new Document("$set", document2), updateOptions);

		// Document document2 = new Document();
		// document2.put("_id", id);
		// document2.put("long_url", longUrl);
		// document2.put("date", SettingsClass.sentDateFormat.format(new
		// Date()));
		//
		// try {
		// document.insertOne(document2);
		// } catch (Exception e) {
		// }

	}

	public static void closeMongoDBConnection() {
		try {
			mongoClient.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void readStatsFromMemCache() {

		System.out.println("reading from memcahce");
		for (Entry<String, DashBoardNameWiseStatsModel> entrySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {

			String dashboardName = entrySet.getKey();
			DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashboardName);
			dashboardName = dashboardName.replace(" ", "_");

			try {
				String totalUsersCount = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.totalUserKey);
				if (totalUsersCount != null && dashBoardNameWiseStatsModel.totalUsersCount.equalsIgnoreCase("0"))
					dashBoardNameWiseStatsModel.totalUsersCount = totalUsersCount;
			} catch (Exception e) {
				e.printStackTrace();
			}
			// this is by pawan we are fetching data from memcache
			try {
				String key = dashboardName.trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;
				String lastNoJobsFound = (String) SettingsClass.memcacheObj.get(key);

				System.out.println("lastNoJobsFound from memcache=" + lastNoJobsFound);
				if (lastNoJobsFound != null) {
					SettingsClass.nojobs_found_users_map.put(key, lastNoJobsFound);

				} else
					readLiveStatsFile = true;

			} catch (NumberFormatException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			try {
				String lastUserProcessed = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.userProcessed);
				if (lastUserProcessed != null)
					dashBoardNameWiseStatsModel.userProcessingCount.set(Integer.parseInt(lastUserProcessed));
				else
					readLiveStatsFile = true;
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				String cloudSearchUsersprocessed = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.cloudSearchUsersPer);
				if (cloudSearchUsersprocessed != null)
					dashBoardNameWiseStatsModel.cloudSearchJobPerc = (Float.parseFloat(cloudSearchUsersprocessed));
				else
					readLiveStatsFile = true;
			} catch (Exception e) {
			}

			try {
				String lastEmailSent = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.emailSend);
				if (lastEmailSent != null)
					dashBoardNameWiseStatsModel.totalEmailCount.set(Integer.parseInt(lastEmailSent));
				else
					readLiveStatsFile = true;
			} catch (Exception e) {
			}
			try {
				String lastNewJobInMail = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.numberofNewJobMailsKey);
				if (lastNewJobInMail != null)
					dashBoardNameWiseStatsModel.numberOfNewJobMails.set(Integer.parseInt(lastNewJobInMail));
				else
					readLiveStatsFile = true;
			} catch (Exception e) {

			}
			try {
				String jobs_per = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.jobsCountInMails);
				if (jobs_per != null) {
					dashBoardNameWiseStatsModel.jobsCountArray = CommonWhiteLabelMainClass.mGson.fromJson(jobs_per, AtomicIntegerArray.class);
				} else
					readLiveStatsFile = true;
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				String csUserProcessingCount = "" + SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.csUserProcessingCount);
				if (!csUserProcessingCount.equalsIgnoreCase("null"))
					dashBoardNameWiseStatsModel.csUserProcessingCount.set(Integer.parseInt(csUserProcessingCount));
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			// get groupAndTemplateCount value for hashmap
			try {

				String groupTemplateCount_arr = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.groupTemplatesCountKey);

				if (groupTemplateCount_arr != null) {
					Utility.groupTemplateIds_toMap(groupTemplateCount_arr);
				} else {

					readLiveStatsFile = true;
				}
				try {
					String duplicateUser = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.duplicateUserKey);
					if (duplicateUser != null) {
						dashBoardNameWiseStatsModel.duplicateUser.set(Integer.parseInt(duplicateUser));
					}

				} catch (Exception e) {

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				String json = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.JobProvidersjobsCountKey);
				if (json != null) {
					dashBoardNameWiseStatsModel.jobProvidersJobCountMap = mGson.fromJson(json, new TypeToken<LinkedHashMap<String, Integer>>() {
					}.getType());

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				String json = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.JobProvidersjobsCountKeyForNoKeyword);
				if (json != null) {
					dashBoardNameWiseStatsModel.jobProvidersJobCountMapForNoKeyword = mGson.fromJson(json, new TypeToken<LinkedHashMap<String, Integer>>() {
					}.getType());

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {

				String duplicateUser = (String) SettingsClass.memcacheObj.get(dashboardName + "_" + SettingsClass.queuName + "_" + SettingsClass.elasticDublicateKey);
				if (duplicateUser != null) {
					dashBoardNameWiseStatsModel.duplicateUserFromElasticCache.set(Integer.parseInt(duplicateUser));
				}
			} catch (Exception e) {

			}
		}

		try {
			String json = (String) SettingsClass.memcacheObj.get(SettingsClass.dashboradFileName.replace(" ", "_") + "_" + SettingsClass.child_Process_stats_hashMap_key);
			if (json != null) {
				SettingsClass.child_Process_stats_hashMap = mGson.fromJson(json, new TypeToken<LinkedHashMap<String, Integer>>() {
				}.getType());
			} else {
				readLiveChildStatsFile = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// jobprovider count as per dthe domain
		try {
			String jobsCount_arr = (String) SettingsClass.memcacheObj.get(SettingsClass.new_jobProviderCountDomainWiseKey);
			if (jobsCount_arr != null) {
				Utility.convert_JobDomainwiseProviderCount_ToHashMap(jobsCount_arr);
			} else {
				readLiveStatsFile = true;
			}
		} catch (Exception e) {

		}

	}

	private static void readStatsFromFile() {
		BufferedReader bufferedReader = null;
		String readLine = null;
		Boolean processRestarted = false;
		try {
			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "dashboard" + File.separator + SettingsClass.queuName + "_live_stats.txt"));
			readLine = bufferedReader.readLine();

			while (readLine != null) {
				String Data[] = readLine.toString().split("\\|");
				System.out.println(readLine);
				String dashBoardName = Data[9];

				DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashBoardName);

				if (dashBoardNameWiseStatsModel == null) {
					readLine = bufferedReader.readLine();
					continue;
				}

				int runningState = Integer.parseInt(Data[10]);
				boolean TodayDate = true;
				Date date1 = null, date2 = null;
				Calendar calendar = new GregorianCalendar();
				String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DATE));
				try {
					date1 = SettingsClass.dateFormat.parse(currentdate);
					date2 = SettingsClass.dateFormat.parse(Data[0]);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (date1.equals(date2)) {
					TodayDate = true;
				} else {
					TodayDate = false;
				}

				if (runningState == 0 && TodayDate) {

					// process has restarted
					processRestarted = true;

					String key = dashBoardName.trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;

					try {
						SettingsClass.nojobs_found_users_map.put(key, Data[17]);
						SettingsClass.memcacheObj.set(key, 0, Data[17]);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						SettingsClass.nojobs_found_users_map.put(key, "1");
					}

					if (dashBoardNameWiseStatsModel.totalUsersCount.equalsIgnoreCase("0"))
						dashBoardNameWiseStatsModel.totalUsersCount = "" + Data[1];

					if (dashBoardNameWiseStatsModel.userProcessingCount.get() == 0)
						dashBoardNameWiseStatsModel.userProcessingCount.set(Integer.parseInt(Data[2]));

					if (dashBoardNameWiseStatsModel.totalEmailCount.get() == 0)
						dashBoardNameWiseStatsModel.totalEmailCount.set(Integer.parseInt(Data[4]));

					if (dashBoardNameWiseStatsModel.numberOfNewJobMails.get() == 0)
						dashBoardNameWiseStatsModel.numberOfNewJobMails.set(Integer.parseInt(Data[11]));

					// CLOUD SEARCH USERS PERCENTAGE
					double usersProcssedCS = Double.parseDouble(Data[7]);
					Integer CSUsersCount = (int) ((usersProcssedCS / 100) * dashBoardNameWiseStatsModel.userProcessingCount.get());

					dashBoardNameWiseStatsModel.user_CloudSearch_ProcessingCount.set(CSUsersCount);
				}

				BufferedReader bufferedReader1 = null;
				String readLine1 = "";

				if (processRestarted) {
					try {
						bufferedReader1 = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "Jobs_count" + File.separator + SettingsClass.queuName + "_process_stats.txt"));
						readLine1 = bufferedReader1.readLine();
						String Data1[] = readLine1.toString().split("\\|");

						String jobC_arr[] = Data1[1].split(",");
						for (int i = 0; i < jobC_arr.length; i++) {
							dashBoardNameWiseStatsModel.jobsCountArray.set(i, Integer.parseInt(jobC_arr[i].trim()));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					bufferedReader1 = null;
					try {
						bufferedReader1 = new BufferedReader(new FileReader(SettingsClass.filePath + "jobProvidersJobCount/" + dashBoardName + "_providersJobsCount.json"));
						readLine1 = bufferedReader1.readLine();

						JobProviderJobCountData JobProviderJobCountObj = new JobProviderJobCountData();
						JobProviderJobCountObj = new Gson().fromJson(readLine1, new TypeToken<JobProviderJobCountData>() {
						}.getType());
						try {
							dashBoardNameWiseStatsModel.jobProvidersJobCountMap.putAll(JobProviderJobCountObj.getSources());
						} catch (Exception e) {
							e.printStackTrace();
						}
					} catch (Exception e) {

					}
				}
				CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.put(dashBoardName, dashBoardNameWiseStatsModel);

				readLine = bufferedReader.readLine();
			}

		} catch (Exception e) {

		}
		// if condition ended process restarted

		bufferedReader = null;

		try {

			for (Entry<String, ChildProcessModel> entrySet : SettingsClass.childProcessModelHashMap.entrySet()) {

				String child_subject_From_File_Names = SettingsClass.childProcessModelHashMap.get(entrySet.getKey()).child_subject_From_File_Names;
				try {
					bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + File.separator + "java_subject_line_stats" + File.separator + SettingsClass.queuName + "_"
							+ child_subject_From_File_Names + ".json"));
					readLine = bufferedReader.readLine();
					Utility.templateGsonToMap_New_Method(readLine, child_subject_From_File_Names);
				} catch (Exception e) {
					continue;
				}

			}
		} catch (Exception e) {

		}

	}

	private static void readChildDomainsStatsFromFile() {
		BufferedReader bufferedReader = null;
		String readLine = null;
		Boolean processRestarted = false;
		try {
			for (int i = 0; i < CommonWhiteLabelMainClass.childDomainMemCacheArrayList.size(); i++) {

				String childKey = CommonWhiteLabelMainClass.childDomainMemCacheArrayList.get(i);

				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "combine_process_dashboard/" + childKey + "_live_stats.txt"));
				readLine = bufferedReader.readLine();
				String Data[] = readLine.toString().split("\\|");
				int runningState = Integer.parseInt(Data[10]);
				boolean TodayDate = true;
				Date date1 = null, date2 = null;
				Calendar calendar = new GregorianCalendar();
				String currentdate = String.valueOf(calendar.get(Calendar.YEAR)) + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.DATE));
				try {
					date1 = SettingsClass.dateFormat.parse(currentdate);
					date2 = SettingsClass.dateFormat.parse(Data[0]);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				if (date1.equals(date2)) {
					TodayDate = true;
				} else {
					TodayDate = false;
				}
				synchronized (SettingsClass.child_Process_stats_hashMap) {

					if (runningState == 0 && TodayDate) {

						// here we set the stats in the memecache from the file
						processRestarted = true;

						Integer totalUsers = SettingsClass.child_Process_stats_hashMap.get(childKey + "_" + SettingsClass.totalUserKey);
						if (totalUsers == null || totalUsers == 0) {
							SettingsClass.memcacheObj.set(childKey + "_" + SettingsClass.totalUserKey, 0, Integer.parseInt(Data[1]));
							SettingsClass.child_Process_stats_hashMap.put(childKey + "_" + SettingsClass.totalUserKey, Integer.parseInt(Data[1]));
						}

						Integer elasticDuplicate = SettingsClass.child_Process_stats_hashMap.get(childKey + "_" + SettingsClass.elasticDuplicateMemcacheKey);
						if (elasticDuplicate == null || elasticDuplicate == 0) {
							SettingsClass.memcacheObj.set(childKey + "_" + SettingsClass.elasticDuplicateMemcacheKey, 0, Integer.parseInt(Data[18]));
							SettingsClass.child_Process_stats_hashMap.put(childKey + "_" + SettingsClass.elasticDuplicateMemcacheKey, Integer.parseInt(Data[18]));
						}
						Integer userProcessed = SettingsClass.child_Process_stats_hashMap.get(childKey + "_" + SettingsClass.userProcessed);

						if (userProcessed == null || userProcessed == 0) {
							SettingsClass.memcacheObj.set(childKey + "_" + SettingsClass.userProcessed, 0, Integer.parseInt(Data[2]));
							SettingsClass.child_Process_stats_hashMap.put(childKey + "_" + SettingsClass.userProcessed, Integer.parseInt(Data[2]));
						}

						Integer emailSend = SettingsClass.child_Process_stats_hashMap.get(childKey + "_" + SettingsClass.emailSend);

						if (emailSend == null || emailSend == 0) {
							SettingsClass.memcacheObj.set(childKey + "_" + SettingsClass.emailSend, 0, Integer.parseInt(Data[4]));
							SettingsClass.child_Process_stats_hashMap.put(childKey + "_" + SettingsClass.emailSend, Integer.parseInt(Data[4]));
						}
					}
				}
			}

		} catch (Exception e) {

		}
	}

	public int getQueueSize(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessages");
		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = SettingsClass.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessages"));
		} catch (Exception e) {

		}

		return size;

	}

	public int getQueueSizeInFlight(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessagesNotVisible");

		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = SettingsClass.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessagesNotVisible"));
		} catch (Exception e) {

		}

		return size;

	}

	private static void getDataFromQueue() {

		if (!SettingsClass.runOrignalTest && !SettingsClass.isThisTestRun) {

			try {
				SettingsClass.bccSendEmailIntervalCount = (Integer.parseInt(SettingsClass.totalUsersCount) * SettingsClass.bccSendEmailIntervalPercentage) / 100;
			} catch (Exception e) {

			}

		}

		String queueUrl = SettingsClass.baseQueurl + SettingsClass.queuName;

		int queueSize = mainClassObject.getQueueSize(queueUrl);

		System.out.println("\n User Queue Url..." + queueUrl);
		System.out.println("\n QUEUE data size..." + queueSize);
		System.out.println("\n Thread count for this QUEUE ..." + SettingsClass.TOTAL_N0_THREAD);

		if (queueSize == 0 && !SettingsClass.runOrignalTest && !SettingsClass.isThisTestRun) {
			String text = SettingsClass.dashboradFileName + " has 0 data in the queue";
			Utility.sendTextSms(text);
			System.exit(0);
		}

		try {

			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);

			if (SettingsClass.isThisTestRun || SettingsClass.runOrignalTest)
				receiveMessageRequest.setMaxNumberOfMessages(1);
			else
				receiveMessageRequest.setMaxNumberOfMessages(10);
			receiveMessageRequest.getMessageAttributeNames();
			receiveMessageRequest.getAttributeNames();

			// start process mail...
			// if (!SettingsClass.isThisTestRun &&
			// !SettingsClass.runOrignalTest)
			// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(SettingsClass.dashboradFileName
			// + " Process has started", "start");

			for (int i = 0; i < SettingsClass.TOTAL_N0_THREAD; i++) {
				JobSearchThread jobSearchThread = new JobSearchThread(receiveMessageRequest, queueUrl);
				jobSearchThread.start();

			}

		} catch (Exception e) {
			System.out.println("\nException in GetUsersFromQueueThread..." + e.toString());
		}

	}

	private static void createDomainWiseHashMap() {
		String query = "select DomainUrl,MailgunDomainName,FromEmailAddress,SendgridUsername,SendgridPassword,EmailClient,sparkpost_key from whitelabels_process";
		Connection con = openConnection();
		ResultSet rs = null;

		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			rs = pst.executeQuery();

			while (rs.next()) {

				String key = rs.getString("DomainUrl");
				if (key == null)
					continue;

				String emailClient = rs.getString("EmailClient");
				if (emailClient.equalsIgnoreCase("mailgun")) {
					key = rs.getString("MailgunDomainName");
				} else if (emailClient.equalsIgnoreCase("sparkpost")) {
					key = rs.getString("DomainUrl") + "_sparkpost";
				}
				String value = rs.getString("FromEmailAddress") + "| " + rs.getString("SendgridUsername") + "| " + rs.getString("SendgridPassword") + "| " + rs.getString("sparkpost_key");
				CommonWhiteLabelMainClass.doimanWiseHashMap.put(key, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}

	}

	private static void loadExtraJarFiles() {
		File directory = new File(SettingsClass.filePath + "jobslib/jobslib_lib");

		if (directory.exists()) {
			try {
				System.out.println("START execution....");

				if (trackLoadedJars != null && trackLoadedJars.size() > 0) {

					for (File file : directory.listFiles()) {

						if (trackLoadedJars.get(file.getName()) != null)
							continue;
						else {
							URL[] urls = new URL[] { file.toURL() };
							trackLoadedJars.put(file.getName(), new URLClassLoader(urls));
						}

					}
				} else {
					trackLoadedJars = new HashMap<String, ClassLoader>();
					for (File file : directory.listFiles()) {
						URL[] urls = new URL[] { file.toURL() };
						trackLoadedJars.put(file.getName(), new URLClassLoader(urls));
					}

				}
			} catch (Exception e) {
				Utility.sendTextSms(e.getMessage() + " problem occurs in loadExtraJarFiles for " + SettingsClass.dashboradFileName + " processs");
				e.printStackTrace();
			}

			try {
				File file = new File(SettingsClass.filePath + "jobslib/jobslib.jar");
				URL url = file.toURL();
				URL[] urls = new URL[] { url };

				if (mainClassObject.classLoader == null)
					mainClassObject.classLoader = new URLClassLoader(urls);

				if (mainClassObject.jobSearchclass == null)
					mainClassObject.jobSearchclass = mainClassObject.classLoader.loadClass("com.main.JobSearchLibraryMain");

				if (mainClassObject.method == null)
					mainClassObject.method = mainClassObject.jobSearchclass.getDeclaredMethod("enterInLib", new Class[] { String.class });
			} catch (Exception e) {
				Utility.sendTextSms(e.getMessage() + " problem occurs in loadExtraJarFiles for " + SettingsClass.dashboradFileName + " processs");
				e.printStackTrace();
			}

		}
	}

	public void jobSearchList(UsersData mUsersData, int localTemplateCount, int localGroupCount) {

		if (mUsersData.country.equalsIgnoreCase("Canada")) {
			return;
		}

		PassingObjectModel mPassingObjectModel = new PassingObjectModel();
		try {

			// mUsersData.seeAllMatchingUrl = "http://" + mUsersData.domainName
			// + "/jobs.php?q=" + mUsersData.keyword.replace(" ", "%20") + "&l="
			// + mUsersData.zipcode.replace(" ", "%20");

			File file = new File(SettingsClass.filePath + "jobslib/jobslib.jar");
			URL url = file.toURL();
			URL[] urls = new URL[] { url };

			if (mainClassObject.classLoader == null)
				mainClassObject.classLoader = new URLClassLoader(urls);

			if (mainClassObject.jobSearchclass == null)
				mainClassObject.jobSearchclass = mainClassObject.classLoader.loadClass("com.main.JobSearchLibraryMain");

			if (method == null)
				method = mainClassObject.jobSearchclass.getDeclaredMethod("enterInLib", new Class[] { String.class });

			String readModelKey = mUsersData.dashboradFileName;
			if (mUsersData.providerName.toLowerCase().contains("overlapped"))
				readModelKey = "overlapped";

			ReadFeedOrderModel readFeedOrderModel = CommonWhiteLabelMainClass.feedOrderModelWhiteLabelWise.get(readModelKey);

			mPassingObjectModel.setmReadFeedOrderModel(readFeedOrderModel);

			mPassingObjectModel.setmUsersData(mUsersData);

			// problem occur in code

			String input = mGson.toJson(mPassingObjectModel);
			// Invoke method with input
			String str = "";
			try {
				str = (String) method.invoke(mainClassObject.jobSearchclass.newInstance(), input);
			} catch (Exception e1) {
				e1.printStackTrace();

				String data = "getting excetption while invoking=" + input + " and exception=" + e1.getMessage();
				BufferedWriter bufferedWriter = null;
				File new_file = new File(SettingsClass.filePath + "test_combined_code_issue/" + SettingsClass.dashboradFileName + "_exception_string" + ".txt");
				try {
					bufferedWriter = new BufferedWriter(new FileWriter(new_file, true));
					bufferedWriter.write(data);

				} catch (Exception e2) {
					e1.printStackTrace();
				} finally {
					try {
						bufferedWriter.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}

				return;
			}

			if (str == null || str.equalsIgnoreCase("")) {
				String data = "null found in string while invoking new instance for input=" + input;
				BufferedWriter bufferedWriter = null;
				File new_file = new File(SettingsClass.filePath + "test_combined_code_issue/" + SettingsClass.dashboradFileName + "_null_string" + ".txt");
				try {
					bufferedWriter = new BufferedWriter(new FileWriter(new_file, true));
					bufferedWriter.write(data);

				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					try {
						bufferedWriter.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
				return;

			}

			JobsResponseModel jobsResponseModel = mGson.fromJson(str, JobsResponseModel.class);

			ArrayList<Jobs> finalAllJobsList = jobsResponseModel.getFinalAllJobsList();
			readFeedOrderModel.apiResponseTimeHashmap = jobsResponseModel.getApiResponseTimeHashmap();
			readFeedOrderModel.apiUserProcessingCountHashmap = jobsResponseModel.getApiUserProcessingCountHashmap();
			try {
				for (Entry<String, ApiStatusModel> apiresponseCheck : readFeedOrderModel.apiStatusHashmap.entrySet()) {

					if (readFeedOrderModel.apiStatusHashmap.get(apiresponseCheck.getKey()).isStopped()) {
						readFeedOrderModel.apiResponseTimeHashmap.put(apiresponseCheck.getKey(), 0.0);
						readFeedOrderModel.apiUserProcessingCountHashmap.put(apiresponseCheck.getKey(), 0);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("size of jobs list=" + finalAllJobsList.size());
			if (finalAllJobsList.size() != 0) {
				createJobsFormat(finalAllJobsList, mUsersData, localTemplateCount, localGroupCount);
			} else {
				mUsersData.errorMessage = "No JObs Found";
				// SettingsClass.userNotProcessedList.add(mUsersData);

				Integer lastNoJobFoundCount = null;
				String key = mUsersData.getDashboradFileName().trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key;
				try {
					lastNoJobFoundCount = Integer.parseInt(SettingsClass.nojobs_found_users_map.get(key));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					lastNoJobFoundCount = null;
				}

				if (lastNoJobFoundCount == null) {
					lastNoJobFoundCount = 1;
				} else {
					lastNoJobFoundCount = lastNoJobFoundCount + 1;
				}

				SettingsClass.nojobs_found_users_map.put(key, "" + lastNoJobFoundCount);

				SettingsClass.memcacheObj.set(key, 0, String.valueOf(lastNoJobFoundCount));
			}

			// problem occur end here

		} catch (Exception e) {

			String data = e.getStackTrace() + "\n" + new Gson().toJson(mPassingObjectModel) + "\n" + new Gson().toJson(e.getStackTrace()) + "\n";
			data += "=======================================================\n";
			BufferedWriter bufferedWriter = null;
			File file = new File(SettingsClass.filePath + "test_combined_code_issue/" + SettingsClass.dashboradFileName + ".txt");
			try {
				bufferedWriter = new BufferedWriter(new FileWriter(file, true));
				bufferedWriter.write(data);

			} catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				try {
					bufferedWriter.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}

			sendSms(e.getMessage());
			e.printStackTrace();
		}
	}

	public void createJobsFormat(List<Jobs> finalJobList, UsersData userDataObject, int localTemplateCount, int localGroupCount) {
		if (finalJobList.size() > 0) {
			int csJobsCount = 0;

			if (userDataObject.isDistanceLessThan70Miles.toLowerCase().equalsIgnoreCase("yes")) {
				finalJobList = addUpsJobObject(finalJobList);
			}
			DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

			boolean flag = true;

			for (int i = 0; i < finalJobList.size(); i++) {
				if (finalJobList.get(i).getApiOrCS().equalsIgnoreCase(SettingsClass.CS))
					csJobsCount++;

				String feed = finalJobList.get(i).getSourcename();

				Integer integer = dashBoardNameWiseStatsModel.jobProvidersJobCountMap.get(feed);

				if (integer != null) {
					dashBoardNameWiseStatsModel.jobProvidersJobCountMap.put(feed, (integer + 1));
				} else {
					dashBoardNameWiseStatsModel.jobProvidersJobCountMap.put(feed, 1);
				}

				if (finalJobList.get(i).getExactOrSynonym().equalsIgnoreCase("CSNK") && flag
				// ||
				// finalJobList.get(i).getExactOrSynonym().equalsIgnoreCase("J2CNK")
				) {
					flag = false;

					String keyName = "CSNK_TotalUser";

					Integer value = dashBoardNameWiseStatsModel.jobProvidersJobCountMapForNoKeyword.get(keyName);

					if (value != null) {
						dashBoardNameWiseStatsModel.jobProvidersJobCountMapForNoKeyword.put(keyName, (value + 1));
					} else {
						dashBoardNameWiseStatsModel.jobProvidersJobCountMapForNoKeyword.put(keyName, 1);
					}
				}

				// new code
				String keyForhashMap = userDataObject.domainName + "_" + feed;
				if (SettingsClass.new_jobProviderCountDomainWise.get(keyForhashMap) != null) {
					SettingsClass.new_jobProviderCountDomainWise.put(keyForhashMap.trim(), SettingsClass.new_jobProviderCountDomainWise.get(keyForhashMap) + 1);
				} else {
					SettingsClass.new_jobProviderCountDomainWise.put(keyForhashMap, 1);
				}
			}

			// got full 40 jobs from the CS and how many jobs from cs in
			try {
				if (csJobsCount == 40) {
					dashBoardNameWiseStatsModel.user_CloudSearch_ProcessingCount.incrementAndGet();

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				float job_perc = 0;
				job_perc = csJobsCount / 40f * 100f;
				dashBoardNameWiseStatsModel.cloudSearchJobPerc = dashBoardNameWiseStatsModel.cloudSearchJobPerc + job_perc;
			} catch (Exception e) {
			}

			try {
				dashBoardNameWiseStatsModel.jobsCountArray.set(finalJobList.size() - 1, dashBoardNameWiseStatsModel.jobsCountArray.get(finalJobList.size() - 1) + 1);
			} catch (Exception e) {
			}

			Object emailMemchaheObject = null;

			try {

				emailMemchaheObject = SettingsClass.memcacheObj.get(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_"
						+ userDataObject.zipcode.replace(" ", "") + "_" + userDataObject.whitelabel_name.replaceAll(" ", ""));
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (emailMemchaheObject != null && emailMemchaheObject.equals("0")) {
				mCommonMailSendClass.sendEmail(userDataObject, finalJobList, localTemplateCount, localGroupCount);
			} else if (SettingsClass.isForMailtester) {
				mCommonMailSendClass.sendEmail(userDataObject, finalJobList, localTemplateCount, localGroupCount);

			}

		}

	}

	public List<Jobs> addUpsJobObject(List<Jobs> finalJobList) {
		Jobs job = new Jobs();
		job.setTitle("Package Handler Part Time");
		job.setSourcename("UPS");
		job.setZipcode("224630");
		job.setJoburl("https://www.jobs-ups.com/job/louisville/package-handler-part-time/1187/224630/?howheard=P4821");
		job.setEmployer("UPS");
		job.setCity("");
		job.setState("");
		ArrayList<Jobs> finalTempList = new ArrayList<Jobs>();
		finalTempList.add(job);
		finalTempList.addAll(finalJobList);
		if (finalTempList.size() >= 40) {
			finalTempList.remove(finalTempList.size() - 1);
		}
		finalJobList.clear();
		finalJobList.addAll(finalTempList);

		return finalJobList;

	}

	synchronized public void sendSms(String error) {
		if (SettingsClass.errorCount.get() <= 2)
			Utility.sendTextSms(error + " problem occurs in jobSearchList for " + SettingsClass.dashboradFileName);
		else {
			Utility.sendTextSms(SettingsClass.dashboradFileName + " process stopped due to any problem");
			CommonWhiteLabelMainClass.closeMongoDBConnection();
			System.exit(0);
		}
		SettingsClass.errorCount.incrementAndGet();
	}

	private static void createParametersModelFromDB(String processName) {

		String evening = "";
		if (processName.toLowerCase().contains("evening"))
			evening = " Evening";

		Connection con = openConnection();
		ResultSet rs = null;
		PreparedStatement pst = null;
		ParameterModelClass mParameterModelClass = null;
		String query = "select * from whitelabels_process_combine where process_name = '" + processName + "'";
		String processes = "";
		try {
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			if (rs.next()) {
				processes = rs.getString("processes");
				SettingsClass.queuName = rs.getString("QueueName");
				if (!SettingsClass.isThisTestRun)
					SettingsClass.TOTAL_N0_THREAD = Integer.parseInt(rs.getString("ThreadCount"));

				SettingsClass.bccSendEmailIntervalPercentage = Integer.parseInt(rs.getString("BccMailPercentage"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// if (processes.equalsIgnoreCase("")) {
		// Utility.sendTextSms(processName +
		// " not found in whitelabels_process_combine so process stopped");
		// System.exit(0);
		// }

		if (processes.equalsIgnoreCase(""))
			processes = processName;

		String a = "";
		String checkEntryExistOrNot = "";
		String[] processesArray = processes.split(",");

		for (int i = 0; i < processesArray.length; i++) {
			a += "'" + processesArray[i] + "',";
		}
		a = a.substring(0, a.length() - 1);

		query = "select * from whitelabels_process where DashboardName in(" + a + ")";
		try {
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				mParameterModelClass = new ParameterModelClass();
				String key = rs.getString("DomainUrl") + "_" + rs.getString("whitelabel_name");

				checkEntryExistOrNot += "'" + rs.getString("DashboardName") + "'";

				if (SettingsClass.queuName.equalsIgnoreCase(""))
					SettingsClass.queuName = rs.getString("QueueName");

				try {
					if (!SettingsClass.isThisTestRun && SettingsClass.TOTAL_N0_THREAD == 1)
						SettingsClass.TOTAL_N0_THREAD = Integer.parseInt(rs.getString("ThreadCount"));

					if (!rs.getString("BccMailPercentage").equalsIgnoreCase(""))
						SettingsClass.bccSendEmailIntervalPercentage = Integer.parseInt(rs.getString("BccMailPercentage"));
				} catch (Exception e) {
				}

				String dashBoardName = rs.getString("DashboardName") + evening;
				DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = dashboardNameStatsModelHashmap.get(dashBoardName);
				if (dashBoardNameWiseStatsModel == null)
					dashBoardNameWiseStatsModel = new DashBoardNameWiseStatsModel();
				dashboardNameStatsModelHashmap.put(dashBoardName, dashBoardNameWiseStatsModel);

				JSONObject convert = convert(rs);
				mParameterModelClass = new Gson().fromJson(convert.toString(), ParameterModelClass.class);
				mParameterModelClass.setDashboardName(mParameterModelClass.getDashboardName() + evening);
				whiteLabelDomainWiseDataHashMap.put(key, mParameterModelClass);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}

		String dashboardNames = "";
		for (String process : processesArray) {
			if (!checkEntryExistOrNot.contains("'" + process + "'")) {
				dashboardNames += process + "\n";

			}
		}
		if (!dashboardNames.equalsIgnoreCase("")) {
			Utility.sendTextSms("Following DashboardName not found in whitelabels_process so process stopped \n" + dashboardNames);
			System.exit(0);
		}

	}

	public static JSONObject convert(ResultSet rs) {
		JSONObject obj = new JSONObject();
		try {

			ResultSetMetaData rsmd = rs.getMetaData();

			// if (rs.next()) {
			int numColumns = rsmd.getColumnCount();

			for (int i = 1; i < numColumns + 1; i++) {
				String column_name = rsmd.getColumnName(i);

				try {

					if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
						obj.put(column_name, rs.getArray(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
						obj.put(column_name, rs.getBoolean(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
						obj.put(column_name, rs.getBlob(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
						obj.put(column_name, rs.getDouble(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
						obj.put(column_name, rs.getFloat(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
						obj.put(column_name, rs.getNString(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
						obj.put(column_name, rs.getString(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
						obj.put(column_name, rs.getInt(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
						obj.put(column_name, rs.getDate(column_name));
					} else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
						obj.put(column_name, rs.getTimestamp(column_name));
					} else {
						obj.put(column_name, rs.getObject(column_name));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static Connection openConnection() {
		String HOST_NAME = "oakuserdbinstance.ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";
		String DB_NAME = "oakalerts";
		String username = "gagangill";
		String password = "GiG879#$%";
		String url1 = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url1, username, password);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
		}
	}
}

/**
 * JobSearchThread Class for processing users
 * 
 */

class JobSearchThread extends Thread {

	String company_title = "";
	LinkedHashMap<String, Integer> jobsKeyJobTitleMap = null;
	AtomicInteger localGroupThreadCount = new AtomicInteger(0);
	AtomicInteger localTemplateThreadCount = new AtomicInteger(0);
	ReceiveMessageRequest receiveMessageRequest;
	String userQueueUrl = "";

	boolean flagForNoKeywordUsers = false;

	JobSearchThread(ReceiveMessageRequest receiveMessageRequest, String userQueueUrl) {

		this.receiveMessageRequest = receiveMessageRequest;
		this.userQueueUrl = userQueueUrl;

	}

	@Override
	public void run() {

		while (true) {

			if (SettingsClass.cal.get(Calendar.HOUR_OF_DAY) >= 23 && SettingsClass.cal.get(Calendar.MINUTE) >= 30) {
				SettingsClass.processesIsRunning = 1;
				Utility.updateMemcacheStatsInEnd();
				Utility.killProcess();
			}

			if (SettingsClass.emptyUserMessageCount.get() <= 100) {

				List<Message> messages = SettingsClass.sqs.receiveMessage(receiveMessageRequest).getMessages();
				if (messages.size() == 0) {
					SettingsClass.emptyUserMessageCount.incrementAndGet();
				} else {

					// reset the empty queue count..
					SettingsClass.emptyUserMessageCount.set(0);

					for (Message message : messages) {

						// split the multiple users into array
						String[] usersList = message.getBody().split("\n");

						for (String user : usersList) {

							company_title = "";

							// hashmap for company+title , count(max 3)
							jobsKeyJobTitleMap = new LinkedHashMap<String, Integer>();

							String[] bodyData = user.split("\\|");
							UsersData userDataObject = new UsersData();

							try {
								userDataObject.id = bodyData[0];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}

							try {
								userDataObject.keyword = (URLDecoder.decode(Utility.html2text(bodyData[1].trim()), "UTF-8"));
								if (userDataObject.keyword.contains("$pipe$")) {
									userDataObject.keyword.replace("$pipe$", "|");
								}
							} catch (Exception e2) {
								userDataObject.keyword = (bodyData[1].trim());
								if (userDataObject.keyword.contains("$pipe$")) {
									userDataObject.keyword.replace("$pipe$", "|");
								}
								e2.printStackTrace();
							}

							try {
								userDataObject.zipcode = bodyData[2];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							try {
								userDataObject.email = bodyData[3];
								userDataObject.email = userDataObject.email.replace(" ", "");
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}

							try {
								userDataObject.advertisement_id = bodyData[4];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}

							try {
								if (userDataObject.advertisement_id.equalsIgnoreCase("110woj-csnk") && flagForNoKeywordUsers) {
									userDataObject.cloudOrJ2cNoKeyword = "cloud";
									flagForNoKeywordUsers = false;
								} else {
									userDataObject.cloudOrJ2cNoKeyword = "j2c";
									flagForNoKeywordUsers = true;
								}
							} catch (Exception e) {
							}

							try {
								userDataObject.registration_date = bodyData[5];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							// 5 //6 -- missing
							try {
								userDataObject.city = bodyData[6];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							try {
								userDataObject.state = bodyData[7];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}

							try {
								userDataObject.latitude = bodyData[8];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							try {
								userDataObject.longitude = bodyData[9];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}

							try {
								userDataObject.upperLatitude = bodyData[10];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							try {
								userDataObject.upperLongitude = bodyData[11];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							try {
								userDataObject.lowerLatitude = bodyData[12];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}
							try {
								userDataObject.lowerLongitude = bodyData[13];
							} catch (Exception e2) {
								//
								e2.printStackTrace();
							}

							try {
								userDataObject.isOpener_Clicker = bodyData[14];
							}

							catch (Exception e2) {
								userDataObject.isOpener_Clicker = "0";
							}

							try {
								userDataObject.openTime = bodyData[15];
							} catch (Exception e2) {
								userDataObject.openTime = "";
							}
							try {
								userDataObject.searchKeyword = (URLDecoder.decode(Utility.html2text(bodyData[22].trim()), "UTF-8"));
								if (userDataObject.searchKeyword.contains("$pipe$")) {
									userDataObject.searchKeyword.replace("$pipe$", "|");
								}
							} catch (Exception e2) {
								userDataObject.searchKeyword = (bodyData[22].trim());
								if (userDataObject.searchKeyword.contains("$pipe$")) {
									userDataObject.searchKeyword.replace("$pipe$", "|");
								}
								e2.printStackTrace();
							}

							try {
								userDataObject.isDistanceLessThan70Miles = bodyData[23];
							} catch (Exception e2) {
								userDataObject.isDistanceLessThan70Miles = "NO";
							}

							try {
								userDataObject.scheduleSendForMailgun = bodyData[24];
							} catch (Exception e) {
								e.printStackTrace();

							}

							try {
								userDataObject.firstName = bodyData[17];

								if (userDataObject.firstName == null || userDataObject.firstName.equalsIgnoreCase("") || userDataObject.firstName.equalsIgnoreCase("null")) {
									userDataObject.firstName = "";
								}

							} catch (Exception e2) {
								userDataObject.firstName = "";
							}

							try {
								userDataObject.radius = bodyData[20];
								if (userDataObject.radius == null || userDataObject.radius.equalsIgnoreCase("") || userDataObject.radius.equalsIgnoreCase("null"))
									userDataObject.radius = "30";
							} catch (Exception e2) {

								userDataObject.radius = "30";
							}
							try {
								userDataObject.t2Value = bodyData[29];
							} catch (Exception e2) {
							}
							try {
								userDataObject.jujuChannel = bodyData[30];
							} catch (Exception e2) {
							}
							try {
								userDataObject.liveRamp = bodyData[31];
							} catch (Exception e2) {
							}

							try {
								userDataObject.compainId = bodyData[32];
							} catch (Exception e2) {
							}

							try {
								userDataObject.domainName = bodyData[33];
							} catch (Exception e2) {
							}

							try {
								userDataObject.userSourceNameForHtml = bodyData[27];
							} catch (Exception e2) {
							}

							try {
								userDataObject.providerName = bodyData[28];
							} catch (Exception e2) {
							}

							try {
								userDataObject.categoryProviderForOpenPixels = bodyData[34];
							} catch (Exception e2) {
							}

							try {
								userDataObject.sendGridCategory = bodyData[36];
							} catch (Exception e2) {
							}
							try {
								userDataObject.country = bodyData[37];
							} catch (Exception e2) {
							}

							try {
								userDataObject.campgain_category_mapping_key = bodyData[38];
							} catch (Exception e2) {
							}

							try {
								userDataObject.whitelabel_name = bodyData[40];
							} catch (Exception e2) {
								e2.printStackTrace();
							}

							try {
								String sparkpostKey = "";
								if (userDataObject.sendGridCategory.toLowerCase().contains("sparkpost"))
									sparkpostKey = "_sparkpost";

								String[] spilt = CommonWhiteLabelMainClass.doimanWiseHashMap.get(userDataObject.domainName + sparkpostKey).split("\\|");
								userDataObject.fromDomainName = spilt[0].trim();
								userDataObject.sendGridUserName = spilt[1].trim();
								userDataObject.sendGridUserPassword = spilt[2].trim();
								userDataObject.sparkpost_key = spilt[3].trim();
							} catch (Exception e2) {
							}

							// userDataObject.dashboradFileName =
							// SettingsClass.dashboradFileName;

							String child_process_key = "";
							String tempKey = "";

							if (userDataObject.sendGridCategory.contains("default value")) {
								tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
							} else if (userDataObject.sendGridCategory.contains("sparkpost")) {
								tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
							} else if (userDataObject.sendGridCategory.equalsIgnoreCase("")) {
								tempKey = "_" + userDataObject.compainId.replace(" ", "").trim();
							} else {
								tempKey = "_" + userDataObject.sendGridCategory.replace(" ", "").trim();
							}
							child_process_key = userDataObject.domainName.replace(" ", "").trim() + tempKey.replace(" ", "").trim();

							userDataObject.child_process_key = child_process_key;

							/*
							 * Getting ParameterModelClass Model From Hashmap
							 */

							String key = userDataObject.domainName + "_" + userDataObject.whitelabel_name;

							ParameterModelClass parameterModelClass = CommonWhiteLabelMainClass.whiteLabelDomainWiseDataHashMap.get(key);
							if (parameterModelClass == null) {

								String data = "domain white label key==" + key + "\n" + new Gson().toJson(userDataObject) + "\n" + "\n" + user + "\n";
								data += "=======================================================\n";
								BufferedWriter bufferedWriter = null;
								File file = new File(SettingsClass.filePath + "test_combined_code_issue/" + userDataObject.whitelabel_name + ".txt");
								try {
									bufferedWriter = new BufferedWriter(new FileWriter(file, true));
									bufferedWriter.write(data);

								} catch (Exception e1) {
									e1.printStackTrace();
								} finally {
									try {
										bufferedWriter.close();
									} catch (Exception e2) {
										e2.printStackTrace();
									}
								}

								continue;

							} else {
								userDataObject.dashboradFileName = parameterModelClass.getDashboardName();
								userDataObject.parameterModelClassObject = parameterModelClass;
							}

							if (!SettingsClass.isThisTestRun && userDataObject.parameterModelClassObject.isCylconSubProvider()
									&& !CommonWhiteLabelMainClass.instanceId.equalsIgnoreCase("i-0c44166521af2ad95")) {
								try {
									Object duplicateUserEmailUPTo4MailSentObject = null;
									try {
										duplicateUserEmailUPTo4MailSentObject = SettingsClass.elasticMemCacheObj.get(userDataObject.email.replace(" ", "") + "_Oak_Cylcon");
									} catch (Exception e) {

										e.printStackTrace();
									}

									if (duplicateUserEmailUPTo4MailSentObject == null) {
										try {
											OperationFuture<Boolean> result = SettingsClass.elasticMemCacheObj.set(userDataObject.email.replace(" ", "") + "_Oak_Cylcon", 0, 0);
										} catch (Exception e) {
											e.printStackTrace();
										}
									} else if (Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString()) >= 4 && !userDataObject.providerName.contains("-web")) {

										addChildElasticDuplicate(userDataObject);

										continue;
									} else if (Integer.parseInt(duplicateUserEmailUPTo4MailSentObject.toString()) >= 10) {

										addChildElasticDuplicate(userDataObject);
										continue;
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							Object duplicateUserEmailObject = null;
							try {

								duplicateUserEmailObject = SettingsClass.memcacheObj.get(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_"
										+ userDataObject.zipcode.replace(" ", "") + "_" + userDataObject.whitelabel_name.replaceAll(" ", ""));

							} catch (Exception e) {
								//
								e.printStackTrace();
							}
							DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

							if (dashBoardNameWiseStatsModel == null) {
								dashBoardNameWiseStatsModel = new DashBoardNameWiseStatsModel();
								CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.put(userDataObject.dashboradFileName, dashBoardNameWiseStatsModel);
							}

							dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

							if (duplicateUserEmailObject == null) {

								dashBoardNameWiseStatsModel.userProcessingCount.incrementAndGet();
								SettingsClass.newuserProcessingCount.incrementAndGet();

								try {
									SettingsClass.memcacheObj.set(userDataObject.dashboradFileName.replace(" ", "_") + "_" + SettingsClass.userProcessed, 0,
											String.valueOf(dashBoardNameWiseStatsModel.userProcessingCount.get()));

								} catch (Exception e) {
									e.printStackTrace();
								}

								try {

									String childUserProcessedKeyStr = userDataObject.child_process_key + "_" + SettingsClass.userProcessed;
									Integer childUserProcessed = null;
									synchronized (SettingsClass.child_Process_stats_hashMap) {

										try {
											childUserProcessed = SettingsClass.child_Process_stats_hashMap.get(childUserProcessedKeyStr);
											if (childUserProcessed == null)
												childUserProcessed = 0;
											childUserProcessed++;
											SettingsClass.memcacheObj.set(childUserProcessedKeyStr, 0, String.valueOf(childUserProcessed));
											SettingsClass.child_Process_stats_hashMap.put(childUserProcessedKeyStr, childUserProcessed);
										} catch (Exception e) {
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

								try {
									SettingsClass.memcacheObj.set(userDataObject.email.replace(" ", "") + "_" + userDataObject.keyword.replace(" ", "") + "_" + userDataObject.zipcode.replace(" ", "")
											+ "_" + userDataObject.whitelabel_name.replaceAll(" ", ""), 0, String.valueOf("0"));
								} catch (Exception e1) {

								}
								try {
									ArrayList<GroupObject> groupObjectList = SettingsClass.childProcessModelHashMap.get(userDataObject.child_process_key).getLocalGroupList();
									if (groupObjectList == null) {
										String data = "child_grouplist_key  " + child_process_key + "\n" + new Gson().toJson(userDataObject) + "\n" + "\n" + user + "\n";
										data += "=======================================================\n";
										BufferedWriter bufferedWriter = null;
										File file = new File("/var/nfs-93/redirect/mis_logs/test_sujectline_key/" + userDataObject.dashboradFileName + ".txt");
										try {
											bufferedWriter = new BufferedWriter(new FileWriter(file, true));
											bufferedWriter.write(data);

										} catch (Exception e1) {
											e1.printStackTrace();
										} finally {
											try {
												bufferedWriter.close();
											} catch (Exception e2) {
												e2.printStackTrace();
											}
										}
									}

									if (localTemplateThreadCount.get() >= SettingsClass.templateObjectList.size()) {
										localTemplateThreadCount.set(0);

										// increment group id count
										localGroupThreadCount.getAndIncrement();

										if (localGroupThreadCount.get() >= groupObjectList.size())
											localGroupThreadCount.set(0);
									}

									CommonWhiteLabelMainClass.mainClassObject.jobSearchList(userDataObject, localTemplateThreadCount.get(), localGroupThreadCount.get());
									localTemplateThreadCount.getAndIncrement();

								} catch (Exception e) {
									System.out.println("users_id=" + userDataObject.id + " complete data=" + user.toString());
									e.printStackTrace();

								}
							} else {

								if (duplicateUserEmailObject.equals("0")) {
									try {
										ArrayList<GroupObject> groupObjectList = SettingsClass.childProcessModelHashMap.get(userDataObject.child_process_key).getLocalGroupList();
										if (groupObjectList == null) {
											String data = "child_grouplist_key  " + child_process_key + "\n" + new Gson().toJson(userDataObject) + "\n" + "\n" + user + "\n";
											data += "=======================================================\n";
											BufferedWriter bufferedWriter = null;
											File file = new File("/var/nfs-93/redirect/mis_logs/test_sujectline_key/" + userDataObject.dashboradFileName + ".txt");
											try {
												bufferedWriter = new BufferedWriter(new FileWriter(file, true));
												bufferedWriter.write(data);

											} catch (Exception e1) {
												e1.printStackTrace();
											} finally {
												try {
													bufferedWriter.close();
												} catch (Exception e2) {
													e2.printStackTrace();
												}
											}
										}

										if (localTemplateThreadCount.get() >= SettingsClass.templateObjectList.size()) {
											localTemplateThreadCount.set(0);

											// increment group id count
											localGroupThreadCount.getAndIncrement();

											if (localGroupThreadCount.get() >= groupObjectList.size())
												localGroupThreadCount.set(0);
										}

										CommonWhiteLabelMainClass.mainClassObject.jobSearchList(userDataObject, localTemplateThreadCount.get(), localGroupThreadCount.get());
										localTemplateThreadCount.getAndIncrement();

									} catch (Exception e) {
										e.printStackTrace();

									}

								} else {

									dashBoardNameWiseStatsModel.duplicateUser.incrementAndGet();

									synchronized (SettingsClass.child_Process_stats_hashMap) {

										Integer childUserProcessed = null;

										String childDuplicateKey = userDataObject.child_process_key + "_" + SettingsClass.duplicateUserKey;

										try {
											childUserProcessed = SettingsClass.child_Process_stats_hashMap.get(childDuplicateKey);
											if (childUserProcessed == null)
												childUserProcessed = 0;
											childUserProcessed++;
											SettingsClass.memcacheObj.set(childDuplicateKey, 0, String.valueOf(childUserProcessed));
											SettingsClass.child_Process_stats_hashMap.put(childDuplicateKey, childUserProcessed);
										} catch (Exception e) {
										}
									}

									try {
										SettingsClass.memcacheObj.set(userDataObject.dashboradFileName.replace(" ", "_") + "_" + SettingsClass.duplicateUserKey, 0,
												String.valueOf(dashBoardNameWiseStatsModel.duplicateUser.get()));
									} catch (Exception e) {
										//
										e.printStackTrace();
									}
								}
							}

						}

						// deleting the msg after full processing..now we
						// are processing max 125 users in a msg..
						try {
							if (!SettingsClass.isThisTestRun) {
								String messageRecieptHandle = message.getReceiptHandle();
								SettingsClass.sqs.deleteMessage(new DeleteMessageRequest(this.userQueueUrl, messageRecieptHandle));
							}
						} catch (Exception e) {
							//
							e.printStackTrace();
						}

						// here exit from the running process on specific
						// mail count...

						if (SettingsClass.isThisTestRun || SettingsClass.runOrignalTest) {
							// Utility.updateMemcacheStatsInEnd();
							System.out.println("Stopped Process!");
							CommonWhiteLabelMainClass.closeMongoDBConnection();
							Utility.updateFilesData();
							System.exit(0);
						}

					}

				}

			}

			if (SettingsClass.emptyUserMessageCount.get() > 100) {

				if (SettingsClass.emptyQueueCount.get() < 10) {

					// getting queue size from the aws server
					int queueSize = CommonWhiteLabelMainClass.mainClassObject.getQueueSize(userQueueUrl);
					// getting size of flight messages
					int flightQueueSize = CommonWhiteLabelMainClass.mainClassObject.getQueueSizeInFlight(userQueueUrl);

					if (queueSize == 0 && flightQueueSize == 0) {

						SettingsClass.emptyQueueCount.incrementAndGet();
					} else if (queueSize != 0) {

						SettingsClass.emptyQueueCount.set(0);
						SettingsClass.emptyUserMessageCount.set(0);

					} else if (flightQueueSize != 0) {

						SettingsClass.emptyQueueCount.set(0);

						try {
							System.out.println(" \n Process slept!");
							sleep(1000 * 60 * 2);
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

				} else {
					SettingsClass.processesIsRunning = 1;
					Utility.updateMemcacheStatsInEnd();
					Utility.killProcess();

				}

			}
		}
	}

	public void addChildElasticDuplicate(UsersData userDataObject) {

		DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(userDataObject.dashboradFileName);

		try {
			SettingsClass.memcacheObj.set(userDataObject.dashboradFileName.replace(" ", "_") + "_" + SettingsClass.elasticDublicateKey, 0,
					String.valueOf(dashBoardNameWiseStatsModel.duplicateUserFromElasticCache.incrementAndGet()));

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			String elasticchildUserDuplicateKeyStr = userDataObject.child_process_key + "_" + SettingsClass.elasticDuplicateMemcacheKey;
			synchronized (SettingsClass.child_Process_stats_hashMap) {
				try {

					Integer elasticDuplicate = SettingsClass.child_Process_stats_hashMap.get(elasticchildUserDuplicateKeyStr);

					if (elasticDuplicate == null)
						elasticDuplicate = 0;
					elasticDuplicate++;
					SettingsClass.child_Process_stats_hashMap.put(elasticchildUserDuplicateKeyStr, elasticDuplicate);
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

/**
 * 
 * Thread class to Write data in memcache and files
 * 
 */
class WriteApiResponseTimeInFileThread implements Runnable {

	public void run() {
		try {
			while (true) {
				try {
					// for three minute sleep
					Thread.sleep(3 * 60 * 1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Utility.updateMemcacheStatsInEnd();
			} // end of while

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}

/**
 * 
 * Thread class to read usability of Api's in file
 * 
 */

class ReadApiParametersThread implements Runnable {

	String preVersionOverlapped = "";
	ReadFeedOrderModel mReadFeedOrderModel = null;
	public String rotateCpcScore = "1";
	public String searchEndpoint = "";
	public String zipRecuiterStartTimeHours[] = null;
	public LinkedHashMap<String, ScoreBaseFeedModel> scoreBaseFeedModelList = new LinkedHashMap<String, ScoreBaseFeedModel>();

	public void run() {

		String preVersion = "";
		BufferedReader bufferedReader = null;
		boolean isCloudActive = true;
		try {

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "zip recruiter time/mongo_url_length.txt"));
				String line = bufferedReader.readLine();
				String[] spiltedArray = null;
				while (line != null) {
					spiltedArray = line.split("\\|");
					CommonWhiteLabelMainClass.urlMaxLengthForMongo.put(spiltedArray[0], spiltedArray[1]);
					line = bufferedReader.readLine();
				}
				bufferedReader.close();

			} catch (Exception e2) {
				e2.printStackTrace();

			}

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "zip recruiter time/zip_recruiter_time.txt"));
				String line = bufferedReader.readLine();
				String[] spiltedArray = line.split("\\|");
				zipRecuiterStartTimeHours = spiltedArray;

				line = bufferedReader.readLine();

				if (line != null) {
					spiltedArray = line.split("\\|");
					if (spiltedArray[1].toString().equalsIgnoreCase("0"))
						isCloudActive = false;
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "score_base_feed/" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "_score_base_feeds.json"));
				String line = bufferedReader.readLine();
				String data = "";
				while (line != null) {
					data += line;
					line = bufferedReader.readLine();
				}

				List<ScoreBaseFeedModel> asList = Arrays.asList(new Gson().fromJson(data, ScoreBaseFeedModel[].class));

				for (ScoreBaseFeedModel mScoreBaseFeedModel : asList) {
					scoreBaseFeedModelList.put(mScoreBaseFeedModel.getName(), mScoreBaseFeedModel);
				}

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "score_base_feed/rotate_cpc_score.txt"));
				String line = bufferedReader.readLine();
				line = bufferedReader.readLine();
				String[] split = line.split("\\|");
				rotateCpcScore = split[1];

			} catch (Exception e2) {
				e2.printStackTrace();
			}

			bufferedReader = null;

			// ====================================================================================

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "Java_process_stats/" + SettingsClass.queuName + "_size.txt"));
				String line1 = null;
				line1 = bufferedReader.readLine();
				while (line1 != null) {

					String[] totalAndDomainChildMemchahe = line1.split("\\|");
					if (totalAndDomainChildMemchahe[2].toLowerCase().contains("global")) {
						String Globalkey = totalAndDomainChildMemchahe[2].replace(" ", "").trim();
						SettingsClass.totalUsersCount = totalAndDomainChildMemchahe[0].replace(" ", "").trim();
						SettingsClass.totalUserKey = Globalkey + SettingsClass.totalUserKey;
						SettingsClass.processStartTime = Globalkey + SettingsClass.processStartTime;
						SettingsClass.userProcessed = Globalkey + SettingsClass.userProcessed;
						SettingsClass.emailSend = Globalkey + SettingsClass.emailSend;
						SettingsClass.duplicateUserKey = Globalkey + SettingsClass.duplicateUserKey;
						SettingsClass.perSecondsMails = Globalkey + SettingsClass.perSecondsMails;
						SettingsClass.cloudSearchUsersPer = Globalkey + SettingsClass.cloudSearchUsersPer;
						SettingsClass.mailCloudJobsPer = Globalkey + SettingsClass.mailCloudJobsPer;
						SettingsClass.perSecondUsers = Globalkey + SettingsClass.perSecondUsers;
						SettingsClass.cloudSearchUsersPer = Globalkey + SettingsClass.cloudSearchUsersPer;
						SettingsClass.average_percentage_NewJobsMailsKey = Globalkey + SettingsClass.average_percentage_NewJobsMailsKey;
						SettingsClass.numberofNewJobMailsKey = Globalkey + SettingsClass.numberofNewJobMailsKey;
						SettingsClass.jobsCountInMails = Globalkey + SettingsClass.jobsCountInMails;
						SettingsClass.JobProvidersjobsCountKey = Globalkey + SettingsClass.JobProvidersjobsCountKey;
						SettingsClass.groupTemplatesCountKey = Globalkey + SettingsClass.groupTemplatesCountKey;
						SettingsClass.elasticDuplicateMemcacheKey = Globalkey + SettingsClass.elasticDuplicateMemcacheKey;

						try {
							SettingsClass.memcacheObj.set(SettingsClass.totalUserKey, 0, SettingsClass.totalUsersCount);
						} catch (Exception e) {

							e.printStackTrace();
						}
						CommonWhiteLabelMainClass.providersSet.add(totalAndDomainChildMemchahe[3].trim());

					} else {
						String childKeys = totalAndDomainChildMemchahe[1].replace(" ", "").trim() + "_" + totalAndDomainChildMemchahe[2].replace(" ", "").trim();
						CommonWhiteLabelMainClass.childDomainMemCacheArrayList.add(childKeys);

						try {
							ChildProcessModel childProcessModel = SettingsClass.childProcessModelHashMap.get(childKeys);
							if (childProcessModel == null)
								childProcessModel = new ChildProcessModel();
							String totalUsersCount = totalAndDomainChildMemchahe[0].trim();
							childProcessModel.child_subject_From_File_Names = totalAndDomainChildMemchahe[4];
							childProcessModel.postalAddress = totalAndDomainChildMemchahe[5];

							childProcessModel.arberPixel = totalAndDomainChildMemchahe[6];
							childProcessModel.whitelabelName = totalAndDomainChildMemchahe[7];
							try {
								childProcessModel.image_postaladdress = totalAndDomainChildMemchahe[8];
							} catch (Exception e) {
								// TODO Auto-generated catch block
								childProcessModel.image_postaladdress = "";
							}
							ArrayList<GroupObject> readAllSubjectLines = readAllSubjectLines(totalAndDomainChildMemchahe[4]);
							childProcessModel.setLocalGroupList(readAllSubjectLines);
							SettingsClass.child_Process_stats_hashMap.put(childKeys + "_" + SettingsClass.totalUserKey, Integer.parseInt(totalUsersCount));
							SettingsClass.childProcessModelHashMap.put(childKeys, childProcessModel);
						} catch (Exception e1) {
							e1.printStackTrace();
						}

						try {
							String whitelabelName = totalAndDomainChildMemchahe[7];
							String domianName = totalAndDomainChildMemchahe[1];
							String totalUsers = totalAndDomainChildMemchahe[0];

							String dashBoardName = CommonWhiteLabelMainClass.whiteLabelDomainWiseDataHashMap.get(domianName + "_" + whitelabelName).getDashboardName();

							DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashBoardName);

							int totalUsersCount = Integer.parseInt(dashBoardNameWiseStatsModel.totalUsersCount);
							totalUsersCount = totalUsersCount + Integer.parseInt(totalUsers);
							dashBoardNameWiseStatsModel.totalUsersCount = "" + totalUsersCount;
							try {
								SettingsClass.memcacheObj.set(dashBoardName.replace(" ", "_") + "_" + SettingsClass.totalUserKey, 0, totalUsersCount + "");
							} catch (Exception e) {
								e.printStackTrace();
							}

							CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.put(dashBoardName, dashBoardNameWiseStatsModel);

						} catch (Exception e) {
							e.printStackTrace();
						}

						try {
							SettingsClass.memcacheObj.set(childKeys + "_" + SettingsClass.totalUserKey, 0, totalAndDomainChildMemchahe[0].trim());
						} catch (Exception e) {

							e.printStackTrace();
						}
						CommonWhiteLabelMainClass.providersSet.add(totalAndDomainChildMemchahe[3].trim());
					}

					line1 = bufferedReader.readLine();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
				try {
					String totalusers = (String) SettingsClass.memcacheObj.get(SettingsClass.totalUserKey);
					if (totalusers != null && SettingsClass.totalUsersCount.equalsIgnoreCase(""))
						SettingsClass.totalUsersCount = totalusers;
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.domainConfigfilePath));
				String line1 = null;
				line1 = bufferedReader.readLine();
				String Data[] = line1.toString().split("\\|");
				System.out.println("Search endoint : " + Data[1].toString());
				searchEndpoint = Data[1].toString();

			} catch (Exception e2) {
				try {
					searchEndpoint = getSearchEndPoint();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (!isCloudActive) {
				searchEndpoint = "";
			}

			// =======================================================================================

			while (true) {

				String toStopReadingDuplicateFile = "";

				for (Entry<String, ParameterModelClass> entrySet : CommonWhiteLabelMainClass.whiteLabelDomainWiseDataHashMap.entrySet()) {

					mReadFeedOrderModel = null;

					ParameterModelClass parameterModelClassObject = CommonWhiteLabelMainClass.whiteLabelDomainWiseDataHashMap.get(entrySet.getKey());
					String key = parameterModelClassObject.getDashboardName();

					if (toStopReadingDuplicateFile.contains("'" + key + "'")) {
						continue;
					} else {
						toStopReadingDuplicateFile += "'" + key + "'";
					}

					mReadFeedOrderModel = CommonWhiteLabelMainClass.feedOrderModelWhiteLabelWise.get(key);

					if (mReadFeedOrderModel == null)
						mReadFeedOrderModel = new ReadFeedOrderModel();

					mReadFeedOrderModel.searchEndpoint = searchEndpoint;
					mReadFeedOrderModel.zipRecuiterStartTimeHours = zipRecuiterStartTimeHours;
					mReadFeedOrderModel.scoreBaseFeedModelList = scoreBaseFeedModelList;
					mReadFeedOrderModel.rotateCpcScore = rotateCpcScore;

					if (parameterModelClassObject.isForEveningAlerts())
						SettingsClass.feedOrderFolderName = "EveningFeedOrderFiles";
					else
						SettingsClass.feedOrderFolderName = "FeedOrderFilesForDemo";

					// }
					try {

						bufferedReader = new BufferedReader(
								new FileReader(SettingsClass.filePath + SettingsClass.feedOrderFolderName + "/" + parameterModelClassObject.getFeedOrderFileName() + ".txt"));

						// read version line(first line)
						String line = bufferedReader.readLine();
						String textmsg = "";
						String version = "";

						String mailText = "";

						boolean isVersionChange = false;

						if (!preVersion.equalsIgnoreCase(line.toString())) {
							version = line.toString();
							mReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.clear();
							mReadFeedOrderModel.alwaysOnTopFeedsString = "";
							mReadFeedOrderModel.sourceCloudSearchQueryList.clear();

							isVersionChange = true;
						}

						line = bufferedReader.readLine();
						line = bufferedReader.readLine();

						textmsg += line.toString() + "<br/>";

						line = bufferedReader.readLine();

						while (line != null) {
							textmsg += line.toString() + "<br/>";

							String[] spiltedArray = line.split("\\|");
							// try {
							// // if (SettingClass.apiFeedJobscountFlag) {
							// List<String> sourceList =
							// SettingsClass.apiFeedOrderToDbNameMatchMap.get(spiltedArray[0].trim());
							// if (sourceList != null) {
							// for (String source : sourceList) {
							// SettingsClass.feedApiListForProviderWiseSourceCountStats.add(source);
							// }
							// } else if (sourceList == null) {
							// if
							// (parameterModelClassObject.getDashboardName().toLowerCase().contains("nexgoal")
							// &&
							// spiltedArray[0].trim().equalsIgnoreCase("NexGoal"))
							// SettingsClass.feedApiListForProviderWiseSourceCountStats.add(spiltedArray[0].trim()
							// + " Direct");
							// else
							//
							// SettingsClass.feedApiListForProviderWiseSourceCountStats.add(spiltedArray[0].trim());
							// }
							// // }
							// } catch (Exception e) {
							// SettingsClass.feedApiListForProviderWiseSourceCountStats.add(spiltedArray[0].trim());
							// } // add feed order name into list only if it is

							if (isVersionChange) {
								if (spiltedArray[1].toString().trim().equalsIgnoreCase("1")) {

									if (spiltedArray[11].toString().trim().equalsIgnoreCase("1")) {
										mReadFeedOrderModel.alwaysOnTopFeedsString += "# " + spiltedArray[0].toString().trim() + " #";
										if (!spiltedArray[0].toString().trim().toLowerCase().contains("api"))
											mReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());
									}
									mReadFeedOrderModel.maxJobsHashmap.put(spiltedArray[0].toString().trim(), Integer.parseInt(spiltedArray[3].toString().trim()));

									if (!spiltedArray[0].toString().trim().equalsIgnoreCase("Uber") && !spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
										if (parameterModelClassObject.getDashboardName().toLowerCase().contains("topusa") && spiltedArray[0].contains("TopUSAJobs"))
											mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0]);
										else if (parameterModelClassObject.getDashboardName().toLowerCase().contains("college") && spiltedArray[0].toString().trim().contains("CollegeRecruiter"))
											mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
										else if (parameterModelClassObject.getDashboardName().toLowerCase().contains("nexgoal") && spiltedArray[0].toString().trim().contains("NexGoal"))
											mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
										else if (parameterModelClassObject.getDashboardName().toLowerCase().contains("linkus") && spiltedArray[0].toString().trim().contains("Linkus"))
											mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
										else if (parameterModelClassObject.getDashboardName().toLowerCase().contains("oakjob") && spiltedArray[0].toString().trim().equalsIgnoreCase("CareerBliss 72"))
											mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
										else if (parameterModelClassObject.getDashboardName().toLowerCase().contains("viktrejobalerts-wordpress")
												&& spiltedArray[0].toString().trim().equalsIgnoreCase("VCN Single Job Posts"))
											mReadFeedOrderModel.sourceCloudSearchQueryListForCollegeRec.add(spiltedArray[0].toString().trim());
										else
											mReadFeedOrderModel.sourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());

									}

								}
							}

							// Add cpc of J2C No Keyword API even active or not
							if (spiltedArray[0].toString().trim().contains("J2C No Keyword API")) {
								try {
									mReadFeedOrderModel.ApiCPCHashMap.put(
											spiltedArray[0].toString().toLowerCase(),
											spiltedArray[5].trim() + "|" + spiltedArray[6].trim() + "|" + spiltedArray[12].trim() + "|" + spiltedArray[13].trim() + "|" + spiltedArray[14].trim() + "|"
													+ spiltedArray[15].trim() + "|" + spiltedArray[16].trim() + "|" + spiltedArray[17].trim() + "|" + spiltedArray[18].trim() + "|"
													+ spiltedArray[19].trim() + "|" + spiltedArray[20].trim() + "|" + spiltedArray[21].trim() + "|" + spiltedArray[7].trim().replace("null", "0") + "|"
													+ spiltedArray[8].trim().replace("null", "0") + "|" + spiltedArray[9].trim().replace("null", "0") + "|"
													+ spiltedArray[10].trim().replace("null", "0"));

								} catch (Exception e) {
									e.printStackTrace();
								}

							}

							if (spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
								if (spiltedArray[1].toString().trim().equalsIgnoreCase("1")) {

									try {
										if (spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
											mReadFeedOrderModel.ApiCPCHashMap.put(
													spiltedArray[0].toString().toLowerCase(),
													spiltedArray[5].trim() + "|" + spiltedArray[6].trim() + "|" + spiltedArray[12].trim() + "|" + spiltedArray[13].trim() + "|"
															+ spiltedArray[14].trim() + "|" + spiltedArray[15].trim() + "|" + spiltedArray[16].trim() + "|" + spiltedArray[17].trim() + "|"
															+ spiltedArray[18].trim() + "|" + spiltedArray[19].trim() + "|" + spiltedArray[20].trim() + "|" + spiltedArray[21].trim() + "|"
															+ spiltedArray[7].trim().replace("null", "0") + "|" + spiltedArray[8].trim().replace("null", "0") + "|"
															+ spiltedArray[9].trim().replace("null", "0") + "|" + spiltedArray[10].trim().replace("null", "0"));

										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									ApiStatusModel apiStatusModelObject = null;

									apiStatusModelObject = mReadFeedOrderModel.apiStatusHashmap.get(spiltedArray[0].toString().trim());

									if (apiStatusModelObject == null)
										apiStatusModelObject = new ApiStatusModel();

									apiStatusModelObject.setActive(true);

									Double responseTime = mReadFeedOrderModel.apiResponseTimeHashmap.get(spiltedArray[0].toString().trim());
									Integer userProcessed = mReadFeedOrderModel.apiUserProcessingCountHashmap.get(spiltedArray[0].toString().trim());

									if (responseTime == null)
										responseTime = 0.0;
									if (userProcessed == null)
										userProcessed = 0;

									if (responseTime / userProcessed > 2000 && !apiStatusModelObject.isStopped()) {
										apiStatusModelObject.setStopped(true);
										apiStatusModelObject.setStopTime(System.currentTimeMillis());

										mReadFeedOrderModel.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
										mReadFeedOrderModel.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);
									} else if (apiStatusModelObject.isStopped() && ((System.currentTimeMillis() - apiStatusModelObject.getStopTime()) / (60 * 1000)) >= 15) {
										apiStatusModelObject.setStopped(false);
										apiStatusModelObject.setStopTime(0);

										mReadFeedOrderModel.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
										mReadFeedOrderModel.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);
									} else {
										apiStatusModelObject.setStopped(false);
										apiStatusModelObject.setStopTime(0);
									}
									mReadFeedOrderModel.apiStatusHashmap.put(spiltedArray[0].toString().trim(), apiStatusModelObject);

								} else {
									ApiStatusModel apiStatusModelObject = mReadFeedOrderModel.apiStatusHashmap.get(spiltedArray[0].toString().trim());
									if (apiStatusModelObject != null) {
										apiStatusModelObject.setActive(true);
										apiStatusModelObject.setStopped(true);
										apiStatusModelObject.setStopTime(0);
										mReadFeedOrderModel.apiStatusHashmap.put(spiltedArray[0].toString().trim(), apiStatusModelObject);
									}

								}
							}

							// new code for new api end

							line = bufferedReader.readLine();
						}

						// if (!mailText.equalsIgnoreCase("")) {
						// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(mailText,
						// "settings");
						// }

						if (isVersionChange) {
							// send mail to tell if SettingsClass has changed
							// if (!preVersion.equalsIgnoreCase("")) {
							// try {
							// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail(textmsg,
							// "settings");
							// } catch (Exception e) {
							// e.printStackTrace();
							// }
							// }
							textmsg = "";
							preVersion = version;
						}

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						try {
							bufferedReader.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					CommonWhiteLabelMainClass.feedOrderModelWhiteLabelWise.put(key, mReadFeedOrderModel);
				}
				try {
					mReadFeedOrderModel = null;
					mReadFeedOrderModel = CommonWhiteLabelMainClass.feedOrderModelWhiteLabelWise.get("overlapped");
					if (mReadFeedOrderModel == null)
						mReadFeedOrderModel = new ReadFeedOrderModel();

					mReadFeedOrderModel.searchEndpoint = searchEndpoint;
					mReadFeedOrderModel.zipRecuiterStartTimeHours = zipRecuiterStartTimeHours;
					mReadFeedOrderModel.scoreBaseFeedModelList = scoreBaseFeedModelList;
					if (rotateCpcScore.equalsIgnoreCase("0"))
						mReadFeedOrderModel.rotateCpcScore = "1";
					else
						mReadFeedOrderModel.rotateCpcScore = "0";

					ReadFeedOrderModel overlappedUserFeedOrder = overlappedUserFeedOrder(mReadFeedOrderModel);
					CommonWhiteLabelMainClass.feedOrderModelWhiteLabelWise.put("overlapped", overlappedUserFeedOrder);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				try {
					Thread.sleep(5 * 60 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {

		}

	}

	public static String getSearchEndPoint() {
		String searchEndpoint = "";
		int counter = 0;
		boolean flag = true;
		BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(SettingsClass.awsAccessKey, SettingsClass.awsSecretKey);
		AmazonSQSClient sqs = new AmazonSQSClient(basicAWSCredentials);
		sqs.setRegion(SettingsClass.region);

		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(SettingsClass.domainQueueUrl);
		receiveMessageRequest.setMaxNumberOfMessages(10);
		receiveMessageRequest.getMessageAttributeNames();
		receiveMessageRequest.getAttributeNames();
		ReceiveMessageResult messageResult;
		while (flag) {
			System.out.println("reading queue");
			messageResult = sqs.receiveMessage(receiveMessageRequest);
			if (messageResult != null) {
				if (messageResult.getMessages().size() != 0) {
					com.amazonaws.services.sqs.model.Message message = messageResult.getMessages().get(0);
					String messData[] = message.getBody().split("\\|");
					searchEndpoint = messData[1].toString();
					flag = false;
				} else {
					counter++;
					if (counter >= 10) {
						// MainWhiteLabelClass.mailSendInterfaceObject.sendStartEndEmail("There
						// is some
						// problem in getting search end point in "
						// + SettingsClass.dashboradFileName, "endpoint");
						flag = false;
					}
				}
			}
		}
		return searchEndpoint;
	}

	private ArrayList<GroupObject> readAllSubjectLines(String fileName) {

		BufferedReader bufferedReader = null;
		ArrayList<GroupObject> localGroupList = new ArrayList<GroupObject>();
		try {
			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + "subjectAndFromName/" + fileName + ".txt"));
			localGroupList = new Gson().fromJson(bufferedReader, new TypeToken<List<GroupObject>>() {
			}.getType());

			for (int i = 0; i < localGroupList.size(); i++) {
				if (localGroupList.get(i).getActive().equalsIgnoreCase("0")) {
					localGroupList.remove(localGroupList.get(i));
					i = i - 1;
				}

			}
		} catch (Exception e) {
			Utility.sendTextSms("Subject From Name File not found " + SettingsClass.filePath + "subjectAndFromName/" + fileName + ".txt Please check...");
		}
		return localGroupList;

	}

	public ReadFeedOrderModel overlappedUserFeedOrder(ReadFeedOrderModel mOverlappedReadFeedOrderModel) {
		BufferedReader bufferedReader = null;
		try {

			bufferedReader = new BufferedReader(new FileReader(SettingsClass.filePath + SettingsClass.feedOrderFolderName + "/Cylcon Overlap Feeders.txt"));

			// read version line(first line)
			String line = bufferedReader.readLine();
			String version = "";

			boolean isVersionChange = false;

			if (!preVersionOverlapped.equalsIgnoreCase(line.toString())) {
				version = line.toString();

				mOverlappedReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.clear();
				mOverlappedReadFeedOrderModel.alwaysOnTopFeedsString = "";
				mOverlappedReadFeedOrderModel.sourceCloudSearchQueryList.clear();

				isVersionChange = true;
			}

			line = bufferedReader.readLine();
			line = bufferedReader.readLine();

			line = bufferedReader.readLine();

			while (line != null) {

				String[] spiltedArray = line.split("\\|");

				if (isVersionChange) {
					if (spiltedArray[1].toString().trim().equalsIgnoreCase("1")) {

						if (spiltedArray[11].toString().trim().equalsIgnoreCase("1")) {
							mOverlappedReadFeedOrderModel.alwaysOnTopFeedsString += "# " + spiltedArray[0].toString().trim() + " #";
							if (!spiltedArray[0].toString().trim().toLowerCase().contains("api"))
								mOverlappedReadFeedOrderModel.alwaysOnTopsourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());
						}
						mOverlappedReadFeedOrderModel.maxJobsHashmap.put(spiltedArray[0].toString().trim(), Integer.parseInt(spiltedArray[3].toString().trim()));

						if (!spiltedArray[0].toString().trim().equalsIgnoreCase("Uber") && !spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
							mOverlappedReadFeedOrderModel.sourceCloudSearchQueryList.add(spiltedArray[0].toString().trim());
						}

					}
				}
				if (spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
					if (spiltedArray[1].toString().trim().equalsIgnoreCase("1")) {

						try {
							if (spiltedArray[0].toString().trim().toLowerCase().contains("api")) {
								mOverlappedReadFeedOrderModel.ApiCPCHashMap.put(spiltedArray[0].toString().toLowerCase(), spiltedArray[5].trim() + "|" + spiltedArray[6].trim() + "|"
										+ spiltedArray[12].trim() + "|" + spiltedArray[13].trim() + "|" + spiltedArray[14].trim() + "|" + spiltedArray[15].trim() + "|" + spiltedArray[16].trim() + "|"
										+ spiltedArray[17].trim() + "|" + spiltedArray[18].trim() + "|" + spiltedArray[19].trim() + "|" + spiltedArray[20].trim() + "|" + spiltedArray[21].trim() + "|"
										+ spiltedArray[7].trim().replace("null", "0") + "|" + spiltedArray[8].trim().replace("null", "0") + "|" + spiltedArray[9].trim().replace("null", "0") + "|"
										+ spiltedArray[10].trim().replace("null", "0"));

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						ApiStatusModel apiStatusModelObject = null;

						apiStatusModelObject = mOverlappedReadFeedOrderModel.apiStatusHashmap.get(spiltedArray[0].toString().trim());

						if (apiStatusModelObject == null)
							apiStatusModelObject = new ApiStatusModel();

						apiStatusModelObject.setActive(true);

						Double responseTime = mOverlappedReadFeedOrderModel.apiResponseTimeHashmap.get(spiltedArray[0].toString().trim());

						Integer userProcessed = mOverlappedReadFeedOrderModel.apiUserProcessingCountHashmap.get(spiltedArray[0].toString().trim());

						if (responseTime == null)
							responseTime = 0.0;
						if (userProcessed == null)
							userProcessed = 0;

						if (responseTime / userProcessed > 2000 && !apiStatusModelObject.isStopped()) {
							apiStatusModelObject.setStopped(true);
							apiStatusModelObject.setStopTime(System.currentTimeMillis());

							mOverlappedReadFeedOrderModel.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
							mOverlappedReadFeedOrderModel.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);

						} else if (apiStatusModelObject.isStopped() && ((System.currentTimeMillis() - apiStatusModelObject.getStopTime()) / (60 * 1000)) >= 15) {
							apiStatusModelObject.setStopped(false);
							apiStatusModelObject.setStopTime(0);

							mOverlappedReadFeedOrderModel.apiResponseTimeHashmap.put(spiltedArray[0].toString().trim(), 0.0);
							mOverlappedReadFeedOrderModel.apiUserProcessingCountHashmap.put(spiltedArray[0].toString().trim(), 0);

						} else {
							apiStatusModelObject.setStopped(false);
							apiStatusModelObject.setStopTime(0);
						}
						mOverlappedReadFeedOrderModel.apiStatusHashmap.put(spiltedArray[0].toString().trim(), apiStatusModelObject);

					} else {
						ApiStatusModel apiStatusModelObject = mReadFeedOrderModel.apiStatusHashmap.get(spiltedArray[0].toString().trim());
						if (apiStatusModelObject != null) {
							apiStatusModelObject.setActive(false);
							apiStatusModelObject.setStopped(true);
							apiStatusModelObject.setStopTime(0);
							mReadFeedOrderModel.apiStatusHashmap.put(spiltedArray[0].toString().trim(), apiStatusModelObject);
						}
					}
				}

				line = bufferedReader.readLine();
			}

			if (isVersionChange) {
				preVersionOverlapped = version;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bufferedReader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mOverlappedReadFeedOrderModel;
	}

}
