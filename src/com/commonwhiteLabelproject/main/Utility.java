package com.commonwhiteLabelproject.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicIntegerArray;

import org.jsoup.Jsoup;

import whitelabels.model.DashBoardNameWiseStatsModel;
import whitelabels.model.FinalGroupObject;
import whitelabels.model.GroupObject;
import whitelabels.model.JobProviderJobCountData;
import whitelabels.model.JobSourceCountModel;
import whitelabels.model.JobsDomain;
import whitelabels.model.ReadFeedOrderModel;
import whitelabels.model.TemplateObject;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.util.EC2MetadataUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Utility {
	/*
	 * =========Send Message==================
	 */
	public static void sendTextSms(String data) {
		try {
			String phoneNumber = "8860547656,8699864636";
			try {
				BufferedReader bufferedReader = new BufferedReader(new FileReader("/var/nfs-93/redirect/mis_logs//update_server_count/phonenumberlist.txt"));
				String line = bufferedReader.readLine();
				if (line != null)
					phoneNumber = line;
				bufferedReader.close();
			} catch (Exception e) {
			}

			data = URLEncoder.encode(data, "UTF-8");
			String url = "http://bhashsms.com/api/sendmsg.php?user=signity1&pass=signity123&sender=RSTRNT&phone=";
			url = url + phoneNumber;
			String text = "&text=" + data;
			url = url + text;
			url = url + "&priority=ndnd&stype=normal";
			System.out.println("Url=" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				System.out.println("Message Send Successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * Kill Process And Stop Server
	 */

	public static synchronized void killProcess() {

		if (!SettingsClass.isProcesskilled) {
			SettingsClass.isProcesskilled = true;
			CommonWhiteLabelMainClass.closeMongoDBConnection();
			Utility.stopServer();
			System.exit(0);

		}

	}

	/*
	 * Stop Servers
	 */

	public static void stopServer() {

		AmazonEC2 ec2;
		AWSCredentials ec2AwsCredentials;
		boolean stopServer = false;
		int processCount = 2;

		try {
			String instanceId = EC2MetadataUtils.getInstanceId();
			System.out.println(" \ninstanceId..." + instanceId);

			if (instanceId.contains(SettingsClass.server98InstanceId)) {
				ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2_98AwsAccessKey, SettingsClass.ec2_98AwsSecretKey);
				ec2 = new AmazonEC2Client(ec2AwsCredentials);
				instanceId = EC2MetadataUtils.getInstanceId();
			} else {
				ec2AwsCredentials = new BasicAWSCredentials(SettingsClass.ec2AwsAccessKey, SettingsClass.ec2AwsSecretKey);
				ec2 = new AmazonEC2Client(ec2AwsCredentials);
			}

			String fileName = "";
			if (instanceId.contains(SettingsClass.server189InstanceId)) {
				fileName = "server189_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server62InstanceId)) {
				fileName = "server62_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server7InstanceId)) {
				fileName = "server7_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server235InstanceId)) {
				fileName = "server235_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server109InstanceId)) {
				fileName = "server109_on_off_stats.txt";
			} else if (instanceId.contains(SettingsClass.server98InstanceId)) {
				fileName = "server98_on_off_stats.txt";
			}

			BufferedReader bufferedReader = null;

			try {
				bufferedReader = new BufferedReader(new FileReader(SettingsClass.killserverFilePath + fileName));

				// bufferedReader = new BufferedReader(new
				// FileReader("/home/sandeep/Desktop/Drive_D/AWS
				// jars/july/"+fileName));

				processCount = Integer.parseInt(bufferedReader.readLine());

				System.out.println(processCount + "");

				if (processCount == 0) {
					stopServer = true;
				} else {
					// decrease process count by 1
					processCount = processCount - 1;

					if (processCount == 0) {
						stopServer = true;
					}
				}

				bufferedReader = null;

			} catch (Exception e2) {
				//
				e2.printStackTrace();
			}

			if ((instanceId.contains(SettingsClass.server189InstanceId) || instanceId.contains(SettingsClass.server62InstanceId) || instanceId.contains(SettingsClass.server7InstanceId)
					|| instanceId.contains(SettingsClass.server235InstanceId) || instanceId.contains(SettingsClass.server109InstanceId) || instanceId.contains(SettingsClass.server98InstanceId))
					&& stopServer) {

				System.out.println(" \n Entered in block of stop server");

				// Stop Ec2 Instance
				StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(instanceId);
				StopInstancesResult stopResult = ec2.stopInstances(stopRequest);

				System.out.println(" \nstopResult..." + stopResult);
			}
			// update process count in file

			BufferedWriter bufferedWriter = null;
			try {
				bufferedWriter = new BufferedWriter(new FileWriter(SettingsClass.killserverFilePath + fileName));

				bufferedWriter.write(String.valueOf(processCount));

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					bufferedWriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String html2text(String html) {
		try {
			return Jsoup.parse(html).text();
		} catch (Exception e) {
		}

		return "";
	}

	public static void groupTemplateIds_toMap(String group_template_count_string) {
		try {
			String group_template_count[] = group_template_count_string.split(",");
			for (int i = 0; i < group_template_count.length; i++) {
				try {
					SettingsClass.templateCountHashMap.put(group_template_count[i].split(":")[0], Integer.parseInt(group_template_count[i].split(":")[1]));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized void updateMemcacheStatsInEnd() {
		try {
			for (Entry<String, DashBoardNameWiseStatsModel> entrySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {

				String dashboardName = entrySet.getKey();
				DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashboardName);
				dashboardName = dashboardName.replace(" ", "_");

				Long TimeDifference = System.currentTimeMillis() - CommonWhiteLabelMainClass.processStartTime;

				try {
					String TimeDifferenceEmailCount = String.valueOf(SettingsClass.newTotalEmailCount.get() / (TimeDifference / 1000));
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.perSecondsMails, 0, TimeDifferenceEmailCount);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.mailCloudJobsPer, 0, String.valueOf(dashBoardNameWiseStatsModel.cloudSearchJobPerc));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				try {
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.cloudSearchUsersPer, 0, String.valueOf(dashBoardNameWiseStatsModel.user_CloudSearch_ProcessingCount.get()));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				try {
					float usersProcssed = SettingsClass.newuserProcessingCount.get() / (TimeDifference / 1000);
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.perSecondUsers, 0, String.valueOf(usersProcssed));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				try {
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.numberofNewJobMailsKey, 0, String.valueOf(dashBoardNameWiseStatsModel.numberOfNewJobMails.get()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					String json = CommonWhiteLabelMainClass.mGson.toJson(dashBoardNameWiseStatsModel.jobsCountArray);
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.jobsCountInMails, 0, json);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				try {
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.csUserProcessingCount, 0, dashBoardNameWiseStatsModel.csUserProcessingCount);
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				try {
					String json = CommonWhiteLabelMainClass.mGson.toJson(dashBoardNameWiseStatsModel.jobProvidersJobCountMap);
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.JobProvidersjobsCountKey, 0, json);

				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					String json = CommonWhiteLabelMainClass.mGson.toJson(dashBoardNameWiseStatsModel.jobProvidersJobCountMapForNoKeyword);
					SettingsClass.memcacheObj.set(dashboardName + "_" + SettingsClass.JobProvidersjobsCountKeyForNoKeyword, 0, json);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			try {
				String json = CommonWhiteLabelMainClass.mGson.toJson(SettingsClass.child_Process_stats_hashMap);
				SettingsClass.memcacheObj.set(SettingsClass.dashboradFileName.replace(" ", "_") + "_" + SettingsClass.child_Process_stats_hashMap_key, 0, json);

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				SettingsClass.memcacheObj.set(SettingsClass.new_jobProviderCountDomainWiseKey, 0, Utility.convert_JobDomainwiseProviderCountStats_ToString());
			} catch (Exception e1) {

				e1.printStackTrace();
			}
			updateFilesData();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void convert_JobDomainwiseProviderCount_ToHashMap(String jobsCount) {
		String jobscount_arr[] = jobsCount.split(",");
		for (int i = 0; i < jobscount_arr.length; i++) {
			String[] jobsSourcePerFeed = jobscount_arr[i].split(":");
			SettingsClass.new_jobProviderCountDomainWise.put(jobsSourcePerFeed[0], Integer.parseInt(jobsSourcePerFeed[1]));
		}
	}

	public static void templateGsonToMap_New_Method(String bufferedReader, String fileName) {
		List<FinalGroupObject> finalGroupObjList = new ArrayList<FinalGroupObject>();
		finalGroupObjList = new Gson().fromJson(bufferedReader, new TypeToken<List<FinalGroupObject>>() {
		}.getType());

		try {

			for (int i = 0; i < finalGroupObjList.size(); i++) {

				if (finalGroupObjList.get(i).getDate().equalsIgnoreCase(SettingsClass.dateFormat.format(new Date()))) {
					for (int temp_id = 0; temp_id < finalGroupObjList.get(i).template.size(); temp_id++) {
						String key = fileName + "_" + finalGroupObjList.get(i).getGroup_id() + "_" + finalGroupObjList.get(i).template.get(temp_id).getTemplateId();
						SettingsClass.templateCountHashMap.put(key, finalGroupObjList.get(i).template.get(temp_id).getTemplateCount());
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String convert_JobDomainwiseProviderCountStats_ToString() {

		String result = "";

		for (String entry : CommonWhiteLabelMainClass.childDomainMemCacheArrayList) {

			String domain = entry.split("_")[0];

			for (Entry<String, Integer> entrySet : SettingsClass.new_jobProviderCountDomainWise.entrySet()) {

				String key = entrySet.getKey();
				String domainName = key.split("_")[0];

				if (domain.equalsIgnoreCase(domainName)) {
					if (SettingsClass.new_jobProviderCountDomainWise.get(key) != null) {
						if (result.equalsIgnoreCase("")) {
							result = SettingsClass.new_jobProviderCountDomainWise.get(key) + "";
						} else {
							result = result + key + ":" + SettingsClass.new_jobProviderCountDomainWise.get(key) + ",";
						}
					} else {
						result = result + key + ":" + 0 + ",";
					}
				}

			}

			// for (String feed :
			// SettingsClass.feedApiListForProviderWiseSourceCountStats) {
			//
			// String key = domain + "_" + feed.trim();
			// if (SettingsClass.new_jobProviderCountDomainWise.get(key) !=
			// null) {
			// if (result.equalsIgnoreCase("")) {
			// result = SettingsClass.new_jobProviderCountDomainWise.get(key) +
			// "";
			// } else {
			// result = result + key + ":" +
			// SettingsClass.new_jobProviderCountDomainWise.get(key) + ",";
			// }
			// } else {
			// result = result + key + ":" + 0 + ",";
			// }
			// }
		}

		return result;
	}

	public static void updateFilesData() {
		FileWriter fileWriterObj = null;

		File log_folder = null;
		File log_dashboard_folder = null;
		File log_process_stats_folder = null;
		File log_jobsProvidersJobCount_stats_folder = null;
		File log_jobsProvidersJobCountForNoKeyword_stats_folder = null;
		File child_log_dashboard_folder = null;
		File log_groupAndTemplateCount_stats_folder = null;
		File new_log_jobsProvidersJobCount_stats_folder = null;
		File userNotProcessedStats_folder = null;

		/**
		 * Write api stats
		 */

		try {
			log_folder = new File(SettingsClass.filePath + SettingsClass.dateFormat.format(Calendar.getInstance().getTime()));
			if (!log_folder.exists()) {
				log_folder.mkdir();
			}
			fileWriterObj = new FileWriter(log_folder + File.separator + SettingsClass.dateFormat.format(new Date()) + "_" + SettingsClass.queuName + ".txt", false);
			String writeString = "";

			for (Entry<String, DashBoardNameWiseStatsModel> entrySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {

				String dashBoardName = entrySet.getKey();
				ReadFeedOrderModel readFeedOrderModel = CommonWhiteLabelMainClass.feedOrderModelWhiteLabelWise.get(entrySet.getKey());

				try {
					writeString += "CloudSearchQuery|"
							+ SettingsClass.decFormat.format((readFeedOrderModel.apiResponseTimeHashmap.get("cloud search") / readFeedOrderModel.apiUserProcessingCountHashmap.get("cloud search")))
							+ "|" + readFeedOrderModel.apiUserProcessingCountHashmap.get("cloud search") + "|" + SettingsClass.df.format(Calendar.getInstance().getTime()) + "|" + dashBoardName + "\n";
				} catch (Exception e) {
					e.printStackTrace();
				}

				for (String feed : readFeedOrderModel.apiStatusHashmap.keySet()) {

					Double respnseTime = readFeedOrderModel.apiResponseTimeHashmap.get(feed);
					Integer usersProcessed = readFeedOrderModel.apiUserProcessingCountHashmap.get(feed);
					double response = 0;
					try {
						if (respnseTime != null && usersProcessed != null) {
							response = respnseTime / usersProcessed;
						}
						writeString += feed + "|" + SettingsClass.decFormat.format(response) + "|" + usersProcessed + "|" + SettingsClass.df.format(Calendar.getInstance().getTime()) + "|"
								+ dashBoardName + "\n";
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
			fileWriterObj.write(writeString);
			fileWriterObj.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		/**
		 * Write Dashboard stats
		 */

		fileWriterObj = null;
		try {
			log_dashboard_folder = new File(SettingsClass.filePath + "dashboard/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
			fileWriterObj = new FileWriter(log_dashboard_folder + File.separator + SettingsClass.queuName + "_live_stats.txt");

			String currentTimeForDashBoard = "";
			if (SettingsClass.processesIsRunning == 1)
				currentTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());
			String writeString = "";
			for (Entry<String, DashBoardNameWiseStatsModel> dashboardNameKeySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {

				DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashboardNameKeySet.getKey());
				Long TimeDifference = System.currentTimeMillis() - CommonWhiteLabelMainClass.processStartTime;

				double cloudSearchJobPerc = dashBoardNameWiseStatsModel.cloudSearchJobPerc / dashBoardNameWiseStatsModel.totalEmailCount.get();
				double usersProcssedCS = ((float) dashBoardNameWiseStatsModel.user_CloudSearch_ProcessingCount.get() / (float) dashBoardNameWiseStatsModel.userProcessingCount.get()) * 100f;
				Long timeInSeconds = (TimeDifference / 1000);
				double perSecondUsersProcssed = SettingsClass.newuserProcessingCount.get() / timeInSeconds;
				String perSecondPercentageEmailCount = String.valueOf(SettingsClass.newTotalEmailCount.get() / timeInSeconds);
				String averageMailsWithNewJob = String
						.valueOf(SettingsClass.decFormat.format(((float) dashBoardNameWiseStatsModel.numberOfNewJobMails.get() / (float) dashBoardNameWiseStatsModel.totalEmailCount.get()) * 100f));

				String no_jobs_found_users = null;
				try {
					no_jobs_found_users = SettingsClass.nojobs_found_users_map.get(dashboardNameKeySet.getKey().trim().replaceAll(" ", "") + "_" + SettingsClass.no_jobs_found_users_key);
					if (no_jobs_found_users == null) {
						no_jobs_found_users = "0";
					}
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					writeString += SettingsClass.dateFormat.format(new Date()) + "|" + dashBoardNameWiseStatsModel.totalUsersCount + "|" + dashBoardNameWiseStatsModel.userProcessingCount.get() + "|"
							+ SettingsClass.decFormat.format(perSecondUsersProcssed) + "|" + dashBoardNameWiseStatsModel.totalEmailCount + "|" + perSecondPercentageEmailCount + "|"
							+ CommonWhiteLabelMainClass.startTimeForDashBoard + "|" + SettingsClass.decFormat.format(usersProcssedCS) + "|" + SettingsClass.decFormat.format(cloudSearchJobPerc) + "|"
							+ dashboardNameKeySet.getKey() + "|" + SettingsClass.processesIsRunning + "|" + dashBoardNameWiseStatsModel.numberOfNewJobMails + "|" + averageMailsWithNewJob + "|"
							+ currentTimeForDashBoard + "|" + dashBoardNameWiseStatsModel.duplicateUser.get() + "|" + dashBoardNameWiseStatsModel.duplicateUserFromElasticCache.get() + "|"
							+ SettingsClass.dashboradFileName + "|" + no_jobs_found_users + "\n";

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			fileWriterObj.write(writeString);
			fileWriterObj.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * Writing job count array
		 */
		fileWriterObj = null;
		try {
			log_process_stats_folder = new File(SettingsClass.filePath + "Jobs_count/");
			if (!log_process_stats_folder.exists()) {
				log_process_stats_folder.mkdir();
			}
			try {
				fileWriterObj = new FileWriter(log_process_stats_folder + File.separator + SettingsClass.queuName + "_process_stats.txt");
				String writeString = "";
				for (Entry<String, DashBoardNameWiseStatsModel> dashboardNameKeySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {
					DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashboardNameKeySet.getKey());
					AtomicIntegerArray jobsCountArray = dashBoardNameWiseStatsModel.jobsCountArray;
					writeString += SettingsClass.dateFormat.format(new Date()) + "|" + Utility.jobsCountInMail_toString(jobsCountArray) + "|" + dashboardNameKeySet.getKey() + "\n";
				}

				fileWriterObj.write(writeString);
			} catch (Exception e) {
				e.printStackTrace();
			}
			fileWriterObj.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * Writing job count array
		 */
		fileWriterObj = null;
		try {
			log_jobsProvidersJobCount_stats_folder = new File(SettingsClass.filePath + "jobProvidersJobCount/");

			if (!log_jobsProvidersJobCount_stats_folder.exists()) {
				log_jobsProvidersJobCount_stats_folder.mkdir();
			}

			try {
				for (Entry<String, DashBoardNameWiseStatsModel> dashboardNameKeySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {
					DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashboardNameKeySet.getKey());

					LinkedHashMap<String, Integer> jobProvidersJobCountMap = dashBoardNameWiseStatsModel.jobProvidersJobCountMap;

					fileWriterObj = new FileWriter(log_jobsProvidersJobCount_stats_folder + File.separator + dashboardNameKeySet.getKey() + "_providersJobsCount.json");
					String writeString = Utility.jobProviderJobCountFinalJsonResult(dashboardNameKeySet.getKey(), jobProvidersJobCountMap) + "\n";
					fileWriterObj.write(writeString);
					fileWriterObj.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			log_jobsProvidersJobCountForNoKeyword_stats_folder = new File(SettingsClass.filePath + "jobProvidersJobCountForNoKeyword/");

			if (!log_jobsProvidersJobCountForNoKeyword_stats_folder.exists()) {
				log_jobsProvidersJobCountForNoKeyword_stats_folder.mkdir();
			}

			try {
				for (Entry<String, DashBoardNameWiseStatsModel> dashboardNameKeySet : CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.entrySet()) {
					DashBoardNameWiseStatsModel dashBoardNameWiseStatsModel = CommonWhiteLabelMainClass.dashboardNameStatsModelHashmap.get(dashboardNameKeySet.getKey());

					LinkedHashMap<String, Integer> jobProvidersJobCountMap = dashBoardNameWiseStatsModel.jobProvidersJobCountMapForNoKeyword;

					if (jobProvidersJobCountMap == null || jobProvidersJobCountMap.isEmpty())
						continue;

					fileWriterObj = new FileWriter(log_jobsProvidersJobCountForNoKeyword_stats_folder + File.separator + dashboardNameKeySet.getKey() + "_providersJobsCount.json");
					String writeString = Utility.jobProviderJobCountFinalJsonResult(dashboardNameKeySet.getKey(), jobProvidersJobCountMap) + "\n";
					fileWriterObj.write(writeString);
					fileWriterObj.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
		}

		try {
			child_log_dashboard_folder = new File(SettingsClass.filePath + "combine_process_dashboard/");
			if (!child_log_dashboard_folder.exists()) {
				child_log_dashboard_folder.mkdir();
			}

			log_groupAndTemplateCount_stats_folder = new File(SettingsClass.filePath + "java_subject_line_stats/");

			if (!log_groupAndTemplateCount_stats_folder.exists()) {
				log_groupAndTemplateCount_stats_folder.mkdir();
			}

			String currentTimeForDashBoard = "";
			if (SettingsClass.processesIsRunning == 1)
				currentTimeForDashBoard = SettingsClass.processTimeDateFormat.format(new Date());

			/*
			 * Child Process Stats
			 */

			// child process
			synchronized (SettingsClass.child_Process_stats_hashMap) {
				String toDuplicateFileWriting = "";
				for (int i = 0; i < CommonWhiteLabelMainClass.childDomainMemCacheArrayList.size(); i++) {

					String childDomainName = CommonWhiteLabelMainClass.childDomainMemCacheArrayList.get(i);

					try {
						fileWriterObj = new FileWriter(child_log_dashboard_folder + File.separator + childDomainName + "_live_stats.txt");
					} catch (Exception e) {

					}

					int totalUsersCount = 0;
					try {
						totalUsersCount = (int) Double.parseDouble("" + SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.totalUserKey));
					} catch (Exception e1) {
					}
					int userProcessingCount = 0;
					try {
						userProcessingCount = (int) Double.parseDouble("" + SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.userProcessed));
					} catch (Exception e1) {
					}
					int totalEmailCount = 0;
					try {
						totalEmailCount = (int) Double.parseDouble("" + SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.emailSend));
					} catch (Exception e1) {
					}
					int elasticDuplicateCount = 0;
					try {
						elasticDuplicateCount = (int) Double.parseDouble("" + SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.elasticDuplicateMemcacheKey));
					} catch (Exception e1) {
					}

					int duplicateCount = 0;
					try {
						duplicateCount = (int) Double.parseDouble("" + SettingsClass.child_Process_stats_hashMap.get(childDomainName + "_" + SettingsClass.duplicateUserKey));
					} catch (Exception e1) {
					}
					try {
						String writeString = SettingsClass.dateFormat.format(new Date()) + "|" + totalUsersCount + "|" + userProcessingCount + "|" + 0 + "|" + totalEmailCount + "|" + 0 + "|"
								+ CommonWhiteLabelMainClass.startTimeForDashBoard + "|" + 0 + "|" + 0 + "|" + childDomainName + "|" + SettingsClass.processesIsRunning + "|" + 0 + "|" + 0 + "|"
								+ currentTimeForDashBoard + "|" + duplicateCount + "|" + elasticDuplicateCount + "|" + SettingsClass.dashboradFileName + "|"
								+ SettingsClass.childProcessModelHashMap.get(childDomainName).whitelabelName + "|" + elasticDuplicateCount;

						fileWriterObj.write(writeString);
						fileWriterObj.close();

						// System.out.println("writing done..........");

					} catch (Exception e) {

						e.printStackTrace();
					}

					// new code to write the
					// stat==================================//==================

					String fileName = SettingsClass.childProcessModelHashMap.get(childDomainName).child_subject_From_File_Names;
					String whiteLableName = SettingsClass.childProcessModelHashMap.get(childDomainName).whitelabelName;
					if (toDuplicateFileWriting.contains("'" + fileName + "'"))
						continue;
					else
						toDuplicateFileWriting += "'" + fileName + "'";

					String resultData = templateGsonResult_New_Method(fileName, childDomainName, whiteLableName);
					System.out.println("file name= for subject lines list" + fileName);

					try {
						fileWriterObj = new FileWriter(log_groupAndTemplateCount_stats_folder + File.separator + SettingsClass.queuName + "_" + fileName + ".json");
						fileWriterObj.write(resultData);
						fileWriterObj.close();
					} catch (Exception e) {

					}
				}

			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		try {
			new_log_jobsProvidersJobCount_stats_folder = new File(SettingsClass.filePath + "new_jobProvidersJobCount/");

			if (!new_log_jobsProvidersJobCount_stats_folder.exists()) {
				new_log_jobsProvidersJobCount_stats_folder.mkdir();
			}
			fileWriterObj = new FileWriter(new_log_jobsProvidersJobCount_stats_folder + File.separator + SettingsClass.queuName + "_providersJobsCount.json");
			String writeString = Utility.convert_jobproviderHashMapStats_toJason() + "\n";
			fileWriterObj.write(writeString);
			fileWriterObj.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			userNotProcessedStats_folder = new File(SettingsClass.filePath + "usersNotProcessedStats/");
			if (!userNotProcessedStats_folder.exists()) {
				userNotProcessedStats_folder.mkdir();
			}
			fileWriterObj = new FileWriter(userNotProcessedStats_folder + "/" + SettingsClass.queuName + SettingsClass.dateFormat.format(new Date()) + ".txt");
			fileWriterObj.write(userNotProcessedString());
			fileWriterObj.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String userNotProcessedString() {

		String userNotProcessedString = "";
		// for (int i = 0; i < SettingsClass.userNotProcessedList.size(); i++) {
		// userNotProcessedString +=
		// SettingsClass.userNotProcessedList.get(i).errorMessage + "|"
		// + SettingsClass.userNotProcessedList.get(i).keyword + "|"
		// + SettingsClass.userNotProcessedList.get(i).email + "|"
		// + SettingsClass.userNotProcessedList.get(i).zipcode + "|"
		// + SettingsClass.userNotProcessedList.get(i).domainName + "|"
		// + SettingsClass.userNotProcessedList.get(i).dashboradFileName + "\n";
		// }
		return userNotProcessedString;
	}

	// for writing in the file we use this method
	public static String convert_jobproviderHashMapStats_toJason() {
		String gson = "";
		String date = SettingsClass.dateFormat.format(new Date());
		List<JobsDomain> finalJobsDomainList = new ArrayList<JobsDomain>();

		String distinctDomainNames = "";

		for (String entry : CommonWhiteLabelMainClass.childDomainMemCacheArrayList) {

			String domain = entry.split("_")[0];
			JobsDomain jobsDomainModel = new JobsDomain();
			jobsDomainModel.setName(domain);
			List<JobSourceCountModel> jobProviderJobCountDataList = new ArrayList<JobSourceCountModel>();

			if (distinctDomainNames.contains("|" + domain + "|"))
				continue;

			distinctDomainNames += "|" + domain + "|";

			for (Entry<String, Integer> entrySet : SettingsClass.new_jobProviderCountDomainWise.entrySet()) {

				String key = entrySet.getKey();

				String[] splitArray = key.split("_");

				String domainName = splitArray[0];

				if (domain.equalsIgnoreCase(domainName)) {
					JobSourceCountModel sourceCountModel = new JobSourceCountModel();
					String sourceName = key.replace(domainName + "_", "");
					sourceCountModel.setSourceName(sourceName);
					sourceCountModel.setCount(SettingsClass.new_jobProviderCountDomainWise.get(key) + "");
					jobProviderJobCountDataList.add(sourceCountModel);
				}
			}

			// for (String sourceName :
			// SettingsClass.feedApiListForProviderWiseSourceCountStats) {
			// JobSourceCountModel sourceCountModel = new JobSourceCountModel();
			// String key = domain + "_" + sourceName;
			// sourceCountModel.setSourceName(sourceName);
			// sourceCountModel.setCount(SettingsClass.new_jobProviderCountDomainWise.get(key)
			// + "");
			// jobProviderJobCountDataList.add(sourceCountModel);
			// }

			jobsDomainModel.setName(domain);
			jobsDomainModel.setJobSourceCountModelList(jobProviderJobCountDataList);
			jobsDomainModel.setDate(date);

			finalJobsDomainList.add(jobsDomainModel);

		}

		gson = new Gson().toJson(finalJobsDomainList);

		return gson;

	}

	public static String jobsCountInMail_toString(AtomicIntegerArray jobsCountArray) {
		String result = jobsCountArray.get(0) + "";
		for (int i = 1; i < jobsCountArray.length(); i++) {
			result = result + "," + jobsCountArray.get(i);
		}
		return result;
	}

	public static String jobProviderJobCountFinalJsonResult(String dashboradFileName, LinkedHashMap<String, Integer> jobProvidersJobCountMap) {
		String gson = "";
		try {
			JobProviderJobCountData JobProviderJobCountObj = new JobProviderJobCountData();
			JobProviderJobCountObj.setDate(SettingsClass.dateFormat.format(new Date()));
			JobProviderJobCountObj.setProcessName(dashboradFileName);
			JobProviderJobCountObj.setSources(jobProvidersJobCountMap);
			gson = new Gson().toJson(JobProviderJobCountObj);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return gson;

	}

	public static String templateGsonResult_New_Method(String fileName, String child_key, String whitelabelName) {
		String gson = "";
		try {

			ArrayList<FinalGroupObject> finalGroupList = new ArrayList<FinalGroupObject>();
			ArrayList<GroupObject> groupList = SettingsClass.childProcessModelHashMap.get(child_key).getLocalGroupList();
			for (int group_id = 0; group_id < groupList.size(); group_id++) {
				FinalGroupObject finalGroupObject = new FinalGroupObject();
				int groupCount = 0;
				for (int temp_id = 0; temp_id < SettingsClass.templateObjectList.size(); temp_id++) {
					String key = fileName + "_" + groupList.get(group_id).getGroupId() + "_" + SettingsClass.templateObjectList.get(temp_id).getTemplateId();
					Integer tempValueObject = SettingsClass.templateCountHashMap.get(key);
					if (tempValueObject == null) {
						tempValueObject = 0;
					}
					TemplateObject template = new TemplateObject();
					template.setTemplateId(SettingsClass.templateObjectList.get(temp_id).getTemplateId());
					template.setTemplateCount(tempValueObject);
					finalGroupObject.template.add(template);
					groupCount = groupCount + tempValueObject;
				}

				finalGroupObject.setGroup_id(groupList.get(group_id).getGroupId());
				finalGroupObject.setFrom_name(groupList.get(group_id).getFromName());
				finalGroupObject.setSubject(groupList.get(group_id).getSubject());
				finalGroupObject.setGroup_count(groupCount);
				finalGroupObject.setProcess_name(whitelabelName);
				finalGroupObject.setDate(SettingsClass.dateFormat.format(new Date()));
				finalGroupList.add(finalGroupObject);
			}
			gson = new Gson().toJson(finalGroupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return gson;

	}
}
